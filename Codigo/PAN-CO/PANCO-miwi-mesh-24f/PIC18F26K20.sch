EESchema Schematic File Version 2
LIBS:74xgxx
LIBS:74xx
LIBS:ac-dc
LIBS:actel
LIBS:adc-dac
LIBS:allegro
LIBS:Altera
LIBS:analog_devices
LIBS:analog_switches
LIBS:atmel
LIBS:audio
LIBS:battery_management
LIBS:bbd
LIBS:bosch
LIBS:brooktre
LIBS:cmos4000
LIBS:cmos_ieee
LIBS:conn
LIBS:contrib
LIBS:cypress
LIBS:dc-dc
LIBS:device
LIBS:digital-audio
LIBS:diode
LIBS:display
LIBS:dsp
LIBS:elec-unifil
LIBS:ESD_Protection
LIBS:ftdi
LIBS:gennum
LIBS:graphic_symbols
LIBS:hc11
LIBS:infineon
LIBS:intel
LIBS:interface
LIBS:intersil
LIBS:ir
LIBS:Lattice
LIBS:leds
LIBS:LEM
LIBS:linear
LIBS:logic_programmable
LIBS:maxim
LIBS:mechanical
LIBS:memory
LIBS:microchip
LIBS:microchip_dspic33dsc
LIBS:microchip_pic10mcu
LIBS:microchip_pic12mcu
LIBS:microchip_pic16mcu
LIBS:microchip_pic18mcu
LIBS:microchip_pic24mcu
LIBS:microchip_pic32mcu
LIBS:microcontrollers
LIBS:modules
LIBS:motor_drivers
LIBS:motorola
LIBS:motors
LIBS:msp430
LIBS:nordicsemi
LIBS:nxp
LIBS:nxp_armmcu
LIBS:onsemi
LIBS:opto
LIBS:Oscillators
LIBS:philips
LIBS:power
LIBS:powerint
LIBS:Power_Management
LIBS:pspice
LIBS:references
LIBS:regul
LIBS:relays
LIBS:rfcom
LIBS:RFSolutions
LIBS:sensors
LIBS:silabs
LIBS:siliconi
LIBS:stm8
LIBS:stm32
LIBS:supertex
LIBS:switches
LIBS:texas
LIBS:transf
LIBS:transistors
LIBS:triac_thyristor
LIBS:ttl_ieee
LIBS:valves
LIBS:video
LIBS:wiznet
LIBS:Worldsemi
LIBS:Xicor
LIBS:xilinx
LIBS:zetex
LIBS:Zilog
LIBS:Mcus
LIBS:PAN-CO-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6000 500  2    60   Input ~ 0
VCCP
Text HLabel 5550 150  0    60   Input ~ 0
VCCD
Text HLabel 3350 4000 0    60   Input ~ 0
VPP
Text HLabel 6850 1600 2    60   Input ~ 0
PGC
Text HLabel 6850 1700 2    60   Input ~ 0
PGD
Text HLabel 6050 2900 2    60   Input ~ 0
GNDD
$Comp
L R R3
U 1 1 5B566E71
P 3450 4300
F 0 "R3" V 3550 4300 50  0000 C CNN
F 1 "R" V 3450 4300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3380 4300 50  0001 C CNN
F 3 "" H 3450 4300 50  0001 C CNN
	1    3450 4300
	-1   0    0    -1  
$EndComp
Text HLabel 3350 4600 0    60   Input ~ 0
VCCD
Text HLabel 3900 3700 0    60   Output ~ 0
SCK
Text HLabel 3550 3800 0    60   Input ~ 0
SDI
Text HLabel 3150 3900 0    60   Output ~ 0
SDO
Text HLabel 4950 1200 0    60   Output ~ 0
CS
$Comp
L R R2
U 1 1 5B566E72
P 1550 2650
F 0 "R2" V 1650 2650 50  0000 C CNN
F 1 "R" V 1550 2650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1480 2650 50  0001 C CNN
F 3 "" H 1550 2650 50  0001 C CNN
	1    1550 2650
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 5B566E73
P 1200 2650
F 0 "R1" V 1300 2650 50  0000 C CNN
F 1 "R" V 1200 2650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 1130 2650 50  0001 C CNN
F 3 "" H 1200 2650 50  0001 C CNN
	1    1200 2650
	1    0    0    -1  
$EndComp
Text HLabel 1200 2150 2    60   Input ~ 0
VCCD
Text HLabel 7500 1100 2    60   Input ~ 0
SDA-i2c
Text HLabel 7500 1200 2    60   Input ~ 0
SDO-i2c
Text HLabel 1550 2900 2    60   Input ~ 0
SDA-i2c
Text HLabel 1200 3050 2    60   Input ~ 0
SDO-i2c
Text Notes 7950 1200 0    60   ~ 0
I2C-SOFT
Text HLabel 4950 1000 0    60   Input ~ 0
AN0
Text HLabel 4950 1100 0    60   Input ~ 0
AN1
Text HLabel 4950 1300 0    60   Input ~ 0
AN3
Text HLabel 4950 1500 0    60   Input ~ 0
AN5
Text HLabel 6850 1000 2    60   Input ~ 0
INT0
Text HLabel 6850 1200 2    60   Input ~ 0
INT3/2
Text HLabel 6850 1100 2    60   Input ~ 0
INT1
Text HLabel 6850 1300 2    60   Input ~ 0
AN9
Text HLabel 4950 1900 0    60   Input ~ 0
AN6
Text HLabel 4950 2000 0    60   Input ~ 0
AN7
Text HLabel 6850 2200 2    60   Input ~ 0
RO1
Text HLabel 6850 2100 2    60   Input ~ 0
DI1
Text Notes 1000 2000 0    60   ~ 0
I2C PULL-UPPS
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
Text HLabel 6850 2000 2    60   Input ~ 0
DE1
Text HLabel 6850 1900 2    60   Input ~ 0
!RE1
NoConn ~ 4950 1000
NoConn ~ 4950 1100
NoConn ~ 4950 1300
NoConn ~ 4950 1500
NoConn ~ 4950 1900
NoConn ~ 4950 2000
$Comp
L C_Small C1
U 1 1 5B566E75
P 1100 1100
F 0 "C1" H 1110 1170 50  0000 L CNN
F 1 "C_Small" H 1110 1020 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 1100 1100 50  0001 C CNN
F 3 "" H 1100 1100 50  0001 C CNN
	1    1100 1100
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C2
U 1 1 5B566E76
P 1500 1100
F 0 "C2" H 1510 1170 50  0000 L CNN
F 1 "CP_Small" H 1510 1020 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_D5.0mm_P2.50mm" H 1500 1100 50  0001 C CNN
F 3 "" H 1500 1100 50  0001 C CNN
	1    1500 1100
	1    0    0    -1  
$EndComp
Text HLabel 1500 1400 2    60   Input ~ 0
GNDD
Text HLabel 1500 800  2    60   Input ~ 0
VCCP
$Comp
L PIC18F26J50_I/SP U1
U 1 1 5B575B69
P 5900 1800
F 0 "U1" H 5200 2750 50  0000 L CNN
F 1 "PIC18F26J50_I/SP" H 6600 2750 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm_Socket_LongPads" H 5800 2900 100 0001 C CNN
F 3 "" H 5900 1750 50  0001 C CNN
	1    5900 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 2200 6850 2200
Wire Wire Line
	6700 2100 6850 2100
Wire Wire Line
	5800 2900 6050 2900
Wire Wire Line
	3350 4000 4100 4000
Wire Wire Line
	3450 4150 3450 4000
Connection ~ 3450 4000
Wire Wire Line
	3350 4600 3450 4600
Wire Wire Line
	3450 4600 3450 4450
Wire Wire Line
	3900 3700 4100 3700
Wire Wire Line
	6850 2300 6700 2300
Wire Wire Line
	1200 2150 1200 2500
Wire Wire Line
	1200 2350 1550 2350
Wire Wire Line
	6700 1100 7500 1100
Wire Wire Line
	6700 1200 7500 1200
Wire Wire Line
	1550 2800 1550 2900
Wire Wire Line
	1200 3050 1200 2800
Wire Notes Line
	7450 1050 7450 1250
Wire Notes Line
	7450 1250 8400 1250
Wire Notes Line
	8400 1250 8400 1050
Wire Notes Line
	8400 1050 7450 1050
Wire Wire Line
	5100 1000 4950 1000
Wire Wire Line
	4950 1100 5100 1100
Wire Wire Line
	5100 1200 4950 1200
Wire Wire Line
	4950 1300 5100 1300
Wire Wire Line
	5100 1400 4950 1400
Wire Wire Line
	4950 1500 5100 1500
Wire Wire Line
	8050 6450 7900 6450
Wire Wire Line
	1550 2350 1550 2500
Connection ~ 1200 2350
Wire Notes Line
	950  2050 950  3150
Wire Notes Line
	950  3150 2050 3150
Wire Notes Line
	2050 3150 2050 2050
Wire Notes Line
	2050 2050 950  2050
Wire Wire Line
	6850 1900 6700 1900
Wire Wire Line
	6850 2000 6700 2000
Wire Wire Line
	4950 1900 5100 1900
Wire Wire Line
	4950 2000 5100 2000
Wire Wire Line
	5550 150  5800 150 
Wire Wire Line
	5800 150  5800 800 
Wire Wire Line
	5800 500  6000 500 
Connection ~ 5800 500 
Connection ~ 5950 500 
Wire Wire Line
	1100 1000 1500 1000
Wire Wire Line
	1100 1200 1500 1200
Wire Wire Line
	1500 1000 1500 800 
Wire Wire Line
	1500 1200 1500 1400
Wire Wire Line
	6700 1700 6850 1700
Wire Wire Line
	5700 2800 5800 2800
Wire Wire Line
	5800 2800 5800 2900
Wire Wire Line
	6850 1300 6700 1300
NoConn ~ 6850 1300
Text HLabel 6850 2500 2    60   Input ~ 0
Wake
Text HLabel 6850 2400 2    60   Input ~ 0
!Reset
Wire Wire Line
	6850 2400 6700 2400
Wire Wire Line
	6850 2500 6700 2500
Wire Wire Line
	6850 1600 6700 1600
$Comp
L R R5
U 1 1 5B59058F
P 4000 3550
F 0 "R5" V 4100 3550 50  0000 C CNN
F 1 "R" V 4000 3550 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3930 3550 50  0001 C CNN
F 3 "" H 4000 3550 50  0001 C CNN
	1    4000 3550
	1    0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 5B5908DF
P 3250 3750
F 0 "R6" V 3350 3750 50  0000 C CNN
F 1 "R" V 3250 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3180 3750 50  0001 C CNN
F 3 "" H 3250 3750 50  0001 C CNN
	1    3250 3750
	-1   0    0    -1  
$EndComp
Wire Wire Line
	3150 3900 4100 3900
Wire Wire Line
	6700 1500 7300 1500
Connection ~ 3250 3900
Connection ~ 4000 3700
Text HLabel 4000 3300 0    60   Input ~ 0
VCCD
Text HLabel 3250 3500 0    60   Input ~ 0
VCCD
Wire Wire Line
	3250 3500 3250 3600
Wire Wire Line
	4000 3400 4000 3300
Text Notes 3100 3450 2    60   ~ 0
PULL-UPPs caso de alta impedancia asegura estado
Text Notes 3050 3750 2    60   ~ 0
PULL-UPPs caso de alta impedancia asegura estado
$Comp
L R R4
U 1 1 5B59870E
P 3650 3650
F 0 "R4" V 3750 3650 50  0000 C CNN
F 1 "R" V 3650 3650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3580 3650 50  0001 C CNN
F 3 "" H 3650 3650 50  0001 C CNN
	1    3650 3650
	1    0    0    1   
$EndComp
Text HLabel 3600 3450 0    60   Input ~ 0
VCCD
Wire Wire Line
	3600 3450 3650 3450
Wire Wire Line
	3650 3450 3650 3500
NoConn ~ 4950 1400
Text HLabel 4950 1400 0    60   Input ~ 0
AN4
$Comp
L PIC24FJ64GA006-I/PT IC1
U 1 1 5B735F09
P 6000 5300
F 0 "IC1" H 4300 7450 50  0000 L CNN
F 1 "PIC24FJ64GA006-I/PT" H 4300 7350 50  0000 L CNN
F 2 "Housings_QFP:TQFP-64_10x10mm_Pitch0.5mm" H 6000 5400 50  0001 C CNN
F 3 "" H 6000 5500 50  0001 C CNN
	1    6000 5300
	1    0    0    -1  
$EndComp
Text HLabel 6350 7650 2    60   Input ~ 0
GNDD
Wire Wire Line
	5650 7500 6300 7500
Connection ~ 5850 7500
Connection ~ 6050 7500
Wire Wire Line
	6300 7500 6300 7650
Wire Wire Line
	6300 7650 6350 7650
Text HLabel 6550 2850 2    60   Input ~ 0
VCCD
Text HLabel 6550 3000 2    60   Input ~ 0
VCCP
Wire Wire Line
	5950 3100 6550 3100
Connection ~ 6150 3100
Connection ~ 6350 3100
Wire Wire Line
	6550 3100 6550 2850
Connection ~ 6500 3100
Wire Wire Line
	3550 3800 4100 3800
Connection ~ 3650 3800
Wire Wire Line
	6700 1000 6850 1000
Text HLabel 8050 6450 2    60   Input ~ 0
INT0
Text HLabel 8700 5750 2    60   Input ~ 0
SDA-i2c
Text HLabel 8700 5650 2    60   Input ~ 0
SDO-i2c
Text Notes 9150 5650 0    60   ~ 0
I2C-SOFT
Text HLabel 8050 5650 2    60   Input ~ 0
INT3/2
Text HLabel 8050 5750 2    60   Input ~ 0
INT1
Wire Wire Line
	7900 5750 8700 5750
Wire Wire Line
	7900 5650 8700 5650
Wire Notes Line
	8650 5800 8650 5600
Wire Notes Line
	8650 5600 9600 5600
Wire Notes Line
	9600 5600 9600 5800
Wire Notes Line
	9600 5800 8650 5800
Text HLabel 4100 4800 0    60   Input ~ 0
PGC
Text HLabel 4100 4900 0    60   Input ~ 0
PGD
Text HLabel 5100 1700 0    60   Input ~ 0
VPP
Text HLabel 7100 2600 0    60   Output ~ 0
SDO
Wire Wire Line
	7100 2600 6700 2600
Text HLabel 7300 1500 2    60   Input ~ 0
SDI
Text HLabel 7100 1400 0    60   Output ~ 0
SCK
Wire Wire Line
	7100 1400 6700 1400
Text HLabel 8050 6550 2    60   Input ~ 0
RO1
Text HLabel 8050 6650 2    60   Input ~ 0
DI1
Text HLabel 8050 6050 2    60   Input ~ 0
DE1
Text HLabel 8050 5950 2    60   Input ~ 0
!RE1
Wire Wire Line
	7900 6550 8050 6550
Wire Wire Line
	7900 6650 8050 6650
Wire Wire Line
	8050 5950 7900 5950
Wire Wire Line
	8050 6050 7900 6050
NoConn ~ 7900 3400
NoConn ~ 7900 3500
NoConn ~ 7900 3600
NoConn ~ 7900 3700
NoConn ~ 7900 3800
NoConn ~ 7900 3900
NoConn ~ 7900 4000
NoConn ~ 7900 4100
NoConn ~ 7900 4200
NoConn ~ 7900 4350
NoConn ~ 7900 4550
NoConn ~ 7900 4450
NoConn ~ 7900 4650
NoConn ~ 7900 4750
NoConn ~ 7900 4850
NoConn ~ 7900 4950
NoConn ~ 7900 5050
NoConn ~ 7900 5150
NoConn ~ 7900 5250
NoConn ~ 7900 5350
NoConn ~ 7900 5550
NoConn ~ 4100 6500
NoConn ~ 4100 6400
NoConn ~ 4100 6300
NoConn ~ 4100 6200
NoConn ~ 4100 6100
NoConn ~ 4100 6000
NoConn ~ 4100 5700
NoConn ~ 4100 5600
NoConn ~ 4100 5500
NoConn ~ 4100 5400
NoConn ~ 4100 5100
NoConn ~ 4100 5000
NoConn ~ 4100 4700
NoConn ~ 4100 4600
NoConn ~ 4100 4500
NoConn ~ 4100 4400
NoConn ~ 4100 4100
NoConn ~ 4100 3400
NoConn ~ 4100 3500
NoConn ~ 4100 3600
NoConn ~ 7900 6250
NoConn ~ 7900 6350
NoConn ~ 6850 2300
$EndSCHEMATC
