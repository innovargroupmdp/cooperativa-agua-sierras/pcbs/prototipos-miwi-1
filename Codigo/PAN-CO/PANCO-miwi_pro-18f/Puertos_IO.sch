EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L PAN-CO-rescue:Conn_01x06_Female J5
U 1 1 5B566CD1
P 8750 4550
F 0 "J5" H 8750 4850 50  0000 C CNN
F 1 "Conn_01x06_Female" H 8750 4150 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 8750 4550 50  0001 C CNN
F 3 "" H 8750 4550 50  0001 C CNN
	1    8750 4550
	1    0    0    -1  
$EndComp
Text Notes 8150 3650 0    60   ~ 0
I/O RS485-1 MODULO: \nAntena
Text HLabel 8000 4000 0    60   Input ~ 0
VCC1
$Comp
L PAN-CO-rescue:Conn_01x05_Female J4
U 1 1 5B566CD2
P 8750 3800
F 0 "J4" H 8750 4000 50  0000 C CNN
F 1 "Conn_01x05_Female" H 8750 3500 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x05_Pitch2.54mm" H 8750 3800 50  0001 C CNN
F 3 "" H 8750 3800 50  0001 C CNN
	1    8750 3800
	1    0    0    -1  
$EndComp
Text HLabel 8000 4350 0    60   Input ~ 0
VCCD
Text HLabel 8000 4450 0    60   Input ~ 0
GNDD
Text HLabel 8000 3800 0    60   Input ~ 0
RS485-B1
Text HLabel 8000 3700 0    60   Input ~ 0
RS485-A1
$Comp
L PAN-CO-rescue:Conn_01x05_Male J2
U 1 1 5B566CD3
P 1800 4200
F 0 "J2" H 1800 4500 50  0000 C CNN
F 1 "Conn_01x05_Male" H 1800 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 1800 4200 50  0001 C CNN
F 3 "" H 1800 4200 50  0001 C CNN
	1    1800 4200
	1    0    0    -1  
$EndComp
Text HLabel 2200 4000 2    60   Input ~ 0
VPP
Text HLabel 2200 4100 2    60   Input ~ 0
VCCP
Text HLabel 2200 4300 2    60   Input ~ 0
PGD
Text HLabel 2200 4400 2    60   Input ~ 0
PGC
Text HLabel 2200 4200 2    60   Input ~ 0
GNDD
Text Notes 1550 3850 0    60   ~ 0
ICSP
Text HLabel 8000 4750 0    60   Input ~ 0
!RE1
Text HLabel 8000 4650 0    60   Input ~ 0
DE1
Text HLabel 8000 4550 0    60   Input ~ 0
DI1
Text HLabel 8000 4850 0    60   Output ~ 0
RO1
Wire Wire Line
	8550 4350 8000 4350
Wire Wire Line
	8550 4450 8000 4450
Wire Notes Line
	9200 5100 8150 5100
Wire Notes Line
	8150 5100 8150 3650
Wire Notes Line
	8150 3650 9200 3650
Wire Notes Line
	9200 3650 9200 5100
Wire Wire Line
	2000 4000 2200 4000
Wire Wire Line
	2200 4100 2000 4100
Wire Wire Line
	2000 4200 2200 4200
Wire Wire Line
	2200 4300 2000 4300
Wire Wire Line
	2200 4400 2000 4400
Wire Notes Line
	1550 4650 2650 4650
Wire Notes Line
	1550 3850 2650 3850
Wire Notes Line
	2650 3850 2650 4650
Wire Notes Line
	1550 3850 1550 4650
Text HLabel 8000 3900 0    60   Input ~ 0
GND1
Text Notes 3550 3950 0    60   ~ 0
SPI-PORT2 (MEM)
Wire Notes Line
	3550 4600 4650 4600
Wire Notes Line
	3550 3950 4650 3950
Wire Notes Line
	4650 3950 4650 4600
Wire Notes Line
	3550 3950 3550 4600
Text Notes 3550 3150 0    60   ~ 0
SPI-PORT1 (MRF)
Wire Notes Line
	3550 3800 4650 3800
Wire Notes Line
	3550 3150 4650 3150
Wire Notes Line
	4650 3150 4650 3800
Wire Notes Line
	3550 3150 3550 3800
Text Notes 4900 3150 0    60   ~ 0
SPI-PORT3 (RF)
Wire Notes Line
	4900 3800 6000 3800
Wire Notes Line
	4900 3150 6000 3150
Wire Notes Line
	6000 3150 6000 3800
Wire Notes Line
	4900 3150 4900 3800
Text HLabel 3700 3700 2    60   Input ~ 0
SCK
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	6200 4650 6200 2800
Wire Notes Line
	6200 2800 3450 2800
Wire Notes Line
	3450 2800 3450 4650
Text Notes 4250 3050 0    51   ~ 0
BAHIAS DE EXPANSION SPI\nSE MONTAN VERTICALMENTE
Wire Notes Line
	10450 5500 6600 5500
Wire Notes Line
	6600 5500 6600 2800
Wire Notes Line
	6600 2800 10450 2800
Wire Notes Line
	10450 2800 10450 5500
Text Notes 7700 3150 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
Wire Notes Line
	3450 4650 6200 4650
Text HLabel 8000 3600 0    60   Input ~ 0
GNDP
Wire Wire Line
	8550 4000 8000 4000
Wire Wire Line
	8550 3900 8000 3900
Wire Wire Line
	8000 3800 8550 3800
Wire Wire Line
	8550 3700 8000 3700
Wire Wire Line
	8550 3600 8000 3600
Wire Wire Line
	8550 4550 8000 4550
Wire Wire Line
	8550 4650 8000 4650
Wire Wire Line
	8550 4750 8000 4750
Wire Wire Line
	8550 4850 8000 4850
Text HLabel 3700 3200 2    60   Input ~ 0
GNDD
Text HLabel 4350 3200 2    60   Input ~ 0
GNDD
Text HLabel 4350 3300 2    60   Input ~ 0
GNDD
Text HLabel 3700 3300 2    60   Input ~ 0
Reset
Text HLabel 3700 3400 2    60   Input ~ 0
Wake
Text HLabel 3700 3500 2    60   Input ~ 0
INT
Text HLabel 4350 3400 2    60   Input ~ 0
VCCD
Text HLabel 4350 3600 2    60   Input ~ 0
CS
Text HLabel 4350 3700 2    60   Input ~ 0
SDO
Text HLabel 3700 3600 2    60   Input ~ 0
SDI
NoConn ~ 4350 3500
$Comp
L PAN-CO-rescue:Conn_01x06_Female J3
U 1 1 5B5A4C4B
P 3500 3400
F 0 "J3" H 3500 3700 50  0000 C CNN
F 1 "Conn_01x06_Female" H 3500 3000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 3500 3400 50  0001 C CNN
F 3 "" H 3500 3400 50  0001 C CNN
	1    3500 3400
	-1   0    0    -1  
$EndComp
$Comp
L PAN-CO-rescue:Conn_01x06_Female J6
U 1 1 5B5A4CCA
P 4150 3400
F 0 "J6" H 4150 3700 50  0000 C CNN
F 1 "Conn_01x06_Female" H 4150 3000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 4150 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0001 C CNN
	1    4150 3400
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
