EESchema Schematic File Version 4
LIBS:Monitor-Caudal-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Monitor-Caudal-rescue:Conn_01x05_Male J2
U 1 1 5B5B5661
P 1800 4200
F 0 "J2" H 1800 4500 50  0000 C CNN
F 1 "Conn_01x05_Male" H 1800 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 1800 4200 50  0001 C CNN
F 3 "" H 1800 4200 50  0001 C CNN
	1    1800 4200
	1    0    0    -1  
$EndComp
Text HLabel 2200 4000 2    60   Input ~ 0
VPP
Text HLabel 2200 4100 2    60   Input ~ 0
VCCP
Text HLabel 2200 4300 2    60   Input ~ 0
PGD
Text HLabel 2200 4400 2    60   Input ~ 0
PGC
Text HLabel 2200 4200 2    60   Input ~ 0
GNDD
Text Notes 1550 3850 0    60   ~ 0
ICSP
Wire Wire Line
	2000 4000 2200 4000
Wire Wire Line
	2000 4100 2900 4100
Wire Wire Line
	2000 4200 2200 4200
Wire Wire Line
	2200 4300 2000 4300
Wire Wire Line
	2200 4400 2000 4400
Wire Notes Line
	1550 4650 2650 4650
Wire Notes Line
	1550 3850 2650 3850
Wire Notes Line
	2650 3850 2650 4650
Wire Notes Line
	1550 3850 1550 4650
Text Notes 4850 3300 0    60   ~ 0
SPI-PORT1 (MRF)
Wire Notes Line
	4150 4200 5250 4200
Wire Notes Line
	4150 3550 5250 3550
Wire Notes Line
	5250 3550 5250 4200
Wire Notes Line
	4150 3550 4150 4200
Text HLabel 4300 4100 2    60   Input ~ 0
SCK
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	6200 4650 6200 2800
Wire Notes Line
	6200 2800 3450 2800
Wire Notes Line
	3450 2800 3450 4650
Text Notes 4250 3050 0    51   ~ 0
BAHIAS DE EXPANSION SPI\nSE MONTAN VERTICALMENTE
Wire Notes Line
	10450 5500 6600 5500
Wire Notes Line
	6600 5500 6600 2800
Wire Notes Line
	6600 2800 10450 2800
Wire Notes Line
	10450 2800 10450 5500
Text Notes 7700 3150 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
Wire Notes Line
	3450 4650 6200 4650
Text HLabel 4300 3800 2    60   Input ~ 0
GNDD
Text HLabel 4300 3500 2    60   Input ~ 0
Reset
Text HLabel 4300 3600 2    60   Input ~ 0
Wake
Text HLabel 4300 4000 2    60   Input ~ 0
INT
Text HLabel 4300 3900 2    60   Input ~ 0
VCCD
Text HLabel 4300 3400 2    60   Input ~ 0
CS
Text HLabel 4300 4200 2    60   Input ~ 0
SDO
Text HLabel 4300 3700 2    60   Input ~ 0
SDI
$Comp
L Monitor-Caudal-rescue:Conn_01x09_Female J3
U 1 1 5B5B5662
P 4100 3800
F 0 "J3" H 4100 4100 50  0000 C CNN
F 1 "Conn_01x09_Female" H 4100 3400 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x09_P1.27mm_Vertical" H 4100 3800 50  0001 C CNN
F 3 "" H 4100 3800 50  0001 C CNN
	1    4100 3800
	-1   0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:R R1
U 1 1 5B6F08F0
P 2900 3950
F 0 "R1" V 2980 3950 50  0000 C CNN
F 1 "R" V 2900 3950 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 2830 3950 50  0001 C CNN
F 3 "" H 2900 3950 50  0001 C CNN
	1    2900 3950
	-1   0    0    1   
$EndComp
Text HLabel 2900 3700 2    60   Input ~ 0
VCCD
Wire Wire Line
	2900 3700 2900 3800
$EndSCHEMATC
