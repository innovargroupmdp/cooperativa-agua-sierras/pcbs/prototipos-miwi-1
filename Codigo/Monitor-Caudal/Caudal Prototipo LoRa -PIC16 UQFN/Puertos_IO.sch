EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Monitor-Caudal-rescue:Conn_01x05_Male J1
U 1 1 5B5B5661
P 1800 4200
F 0 "J1" H 1800 4500 50  0000 C CNN
F 1 "Conn_01x05_Male" H 1800 3900 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x05_P1.27mm_Vertical" H 1800 4200 50  0001 C CNN
F 3 "" H 1800 4200 50  0001 C CNN
	1    1800 4200
	1    0    0    -1  
$EndComp
Text HLabel 2200 4000 2    60   Input ~ 0
VPP
Text HLabel 2200 4100 2    60   Input ~ 0
VCCP
Text HLabel 2200 4300 2    60   Input ~ 0
PGD
Text HLabel 2200 4400 2    60   Input ~ 0
PGC
Text HLabel 2200 4200 2    60   Input ~ 0
GNDD
Text Notes 1550 3850 0    60   ~ 0
ICSP
Wire Wire Line
	2000 4000 2200 4000
Wire Wire Line
	2000 4200 2200 4200
Wire Wire Line
	2200 4300 2000 4300
Wire Wire Line
	2200 4400 2000 4400
Wire Notes Line
	1550 4650 2650 4650
Wire Notes Line
	1550 3850 2650 3850
Wire Notes Line
	2650 3850 2650 4650
Wire Notes Line
	1550 3850 1550 4650
Text Notes 4850 3300 0    60   ~ 0
SPI-PORT1 (MRF)
Wire Notes Line
	4150 4400 5250 4400
Wire Notes Line
	4150 3300 5250 3300
Wire Notes Line
	5250 3550 5250 4200
Wire Notes Line
	3900 3500 3900 4150
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	6200 4650 6200 2800
Wire Notes Line
	6200 2800 3450 2800
Wire Notes Line
	3450 2800 3450 4650
Text Notes 4250 3050 0    51   ~ 0
BAHIAS DE EXPANSION SPI\nSE MONTAN VERTICALMENTE
Wire Notes Line
	10450 5500 6600 5500
Wire Notes Line
	6600 5500 6600 2800
Wire Notes Line
	6600 2800 10450 2800
Wire Notes Line
	10450 2800 10450 5500
Text Notes 7700 3150 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
Wire Notes Line
	3450 4650 6200 4650
Wire Wire Line
	2200 4100 2000 4100
$Comp
L Library-cbas:SX1278-LORA U2
U 1 1 611C644B
P 8400 3450
F 0 "U2" H 8475 3715 50  0000 C CNN
F 1 "SX1278-LORA" H 8475 3624 50  0000 C CNN
F 2 "Monitor-RF_MODULE:sx1278" H 8100 3450 50  0001 C CNN
F 3 "/home/cba/kicad/Library-propia/Doc/SX1278ZTR4-lora.pdf" H 8475 3533 50  0000 C CNN
	1    8400 3450
	1    0    0    -1  
$EndComp
Text HLabel 7650 3500 0    60   Input ~ 0
GNDD
Text HLabel 7650 3650 0    60   Input ~ 0
GNDD
Text HLabel 7650 3800 0    60   Input ~ 0
GNDD
Text HLabel 7650 3950 0    60   Input ~ 0
GNDD
Text HLabel 7650 5050 0    60   Input ~ 0
GNDD
Text HLabel 9300 3500 2    60   Input ~ 0
GNDD
Text HLabel 9300 4650 2    60   Input ~ 0
GNDD
Text HLabel 9300 4800 2    60   Input ~ 0
GNDD
Text HLabel 9300 5100 2    60   Input ~ 0
GNDD
Text HLabel 9300 3900 2    60   Input ~ 0
SCK
Text HLabel 9300 4200 2    60   Input ~ 0
SDI
Text HLabel 9300 4050 2    60   Output ~ 0
SDO
Text HLabel 9300 4500 2    60   Input ~ 0
Reset
Text HLabel 9300 4350 2    60   Input ~ 0
CS
Text HLabel 9300 3700 2    60   Input ~ 0
VCCD
Text HLabel 7650 4100 0    60   Output ~ 0
INT
NoConn ~ 7650 4250
NoConn ~ 7650 4400
NoConn ~ 7650 4550
NoConn ~ 7650 4700
NoConn ~ 7650 4850
$Comp
L Device:Antenna AE1
U 1 1 6119FCE2
P 9900 4750
F 0 "AE1" H 9980 4739 50  0000 L CNN
F 1 "Antenna" H 9980 4648 50  0000 L CNN
F 2 "" H 9900 4750 50  0001 C CNN
F 3 "~" H 9900 4750 50  0001 C CNN
	1    9900 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 4950 9300 4950
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 611AD33E
P 9700 4950
F 0 "J3" H 9808 5131 50  0000 C CNN
F 1 "Conn_01x01_Male" H 9808 5040 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 9700 4950 50  0001 C CNN
F 3 "~" H 9700 4950 50  0001 C CNN
	1    9700 4950
	1    0    0    -1  
$EndComp
Connection ~ 9900 4950
$EndSCHEMATC
