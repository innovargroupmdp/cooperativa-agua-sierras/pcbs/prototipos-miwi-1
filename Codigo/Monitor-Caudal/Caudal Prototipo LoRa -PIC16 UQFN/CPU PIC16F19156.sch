EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5750 2550 2    60   Input ~ 0
VCCP
Text HLabel 5300 2550 0    60   Input ~ 0
VCCD
Text HLabel 3900 3050 0    60   Input ~ 0
VPP
Text HLabel 7750 4550 2    60   Input ~ 0
PGC
Text HLabel 7750 4650 2    60   Input ~ 0
PGD
Text HLabel 6200 5050 2    60   Input ~ 0
GNDD
$Comp
L Monitor-Caudal-rescue:R R2
U 1 1 5B5B5832
P 4000 2750
F 0 "R2" V 4100 2750 50  0000 C CNN
F 1 "R" V 4000 2750 50  0000 C CNN
F 2 "Resistor_SMD:R_0603_1608Metric_Pad0.98x0.95mm_HandSolder" V 3930 2750 50  0001 C CNN
F 3 "" H 4000 2750 50  0001 C CNN
	1    4000 2750
	1    0    0    -1  
$EndComp
Text HLabel 3900 2450 0    60   Input ~ 0
VCCD
Text HLabel 4200 4350 0    60   Input ~ 0
SDI
Text HLabel 7750 3950 2    60   Output ~ 0
CS
Text HLabel 7750 3150 2    60   Input ~ 0
AN1
Text HLabel 7750 4150 2    60   Input ~ 0
INT0
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
$Comp
L Monitor-Caudal-rescue:C_Small C1
U 1 1 5B5B5835
P 2400 2000
F 0 "C1" H 2410 2070 50  0000 L CNN
F 1 "C_Small" H 2410 1920 50  0000 L CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.42x1.75mm_HandSolder" H 2400 2000 50  0001 C CNN
F 3 "" H 2400 2000 50  0001 C CNN
	1    2400 2000
	1    0    0    -1  
$EndComp
Text HLabel 2400 2300 2    60   Input ~ 0
GNDD
Text HLabel 2400 1700 2    60   Input ~ 0
VCCP
Text HLabel 7750 4050 2    60   Input ~ 0
!Reset
NoConn ~ 4650 4150
NoConn ~ 4650 4450
Text HLabel 7750 3750 2    60   Input ~ 0
Vbat
Wire Wire Line
	3900 3050 4000 3050
Wire Wire Line
	4000 2900 4000 3050
Connection ~ 4000 3050
Wire Wire Line
	3900 2450 4000 2450
Wire Wire Line
	4000 2450 4000 2600
Wire Wire Line
	5300 2550 5550 2550
Wire Wire Line
	2400 1900 2400 1700
Wire Wire Line
	2400 2100 2400 2300
Wire Wire Line
	7550 4650 7750 4650
Wire Wire Line
	7750 4550 7550 4550
Wire Wire Line
	7750 3750 7550 3750
Wire Wire Line
	5550 2550 5550 2850
Wire Wire Line
	5550 2550 5750 2550
Wire Wire Line
	4000 3050 4650 3050
Connection ~ 5550 2550
Wire Wire Line
	6000 5050 6200 5050
Wire Wire Line
	5900 4850 6000 4850
Wire Wire Line
	7750 3950 7550 3950
Wire Wire Line
	7750 3150 7550 3150
Wire Wire Line
	6000 4850 6000 5050
NoConn ~ 7550 3350
NoConn ~ 7550 3450
NoConn ~ 7550 3550
NoConn ~ 7550 3650
NoConn ~ 7550 3050
NoConn ~ 7550 4250
NoConn ~ 6100 4850
Text HLabel 4200 4650 0    60   Output ~ 0
SDO
Text HLabel 4200 4550 0    60   Input ~ 0
Wake
Wire Wire Line
	4200 4550 4650 4550
Wire Wire Line
	4650 4650 4200 4650
Text HLabel 8500 4450 2    60   Input ~ 0
PulMed
Text HLabel 8500 4350 2    60   Output ~ 0
En_Hall
$Comp
L Device:Crystal Y1
U 1 1 61117879
P 3100 3950
F 0 "Y1" H 3100 4218 50  0000 C CNN
F 1 "Crystal" H 3100 4127 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 3100 3950 50  0001 C CNN
F 3 "~" H 3100 3950 50  0001 C CNN
	1    3100 3950
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:C_Small C2
U 1 1 61127649
P 2800 4100
F 0 "C2" H 2810 4170 50  0000 L CNN
F 1 "C_Small" H 2810 4020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric_Pad0.74x0.62mm_HandSolder" H 2800 4100 50  0001 C CNN
F 3 "" H 2800 4100 50  0001 C CNN
	1    2800 4100
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:C_Small C6
U 1 1 61127AED
P 3350 4100
F 0 "C6" H 3360 4170 50  0000 L CNN
F 1 "C_Small" H 3360 4020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric_Pad0.74x0.62mm_HandSolder" H 3350 4100 50  0001 C CNN
F 3 "" H 3350 4100 50  0001 C CNN
	1    3350 4100
	1    0    0    -1  
$EndComp
Text HLabel 2800 4300 0    60   Input ~ 0
GNDD
Text HLabel 3350 4300 0    60   Input ~ 0
GNDD
Wire Wire Line
	2800 4200 2800 4300
Wire Wire Line
	3350 4200 3350 4300
Wire Wire Line
	3350 4000 3350 3950
Wire Wire Line
	3350 3950 3250 3950
Wire Wire Line
	2950 3950 2800 3950
Wire Wire Line
	2800 3950 2800 4000
Wire Wire Line
	2800 3950 2800 3800
Wire Wire Line
	2800 3800 3550 3800
Wire Wire Line
	3550 3800 3550 3950
Connection ~ 2800 3950
Wire Wire Line
	3450 4050 3450 3950
Wire Wire Line
	3450 3950 3350 3950
Wire Wire Line
	3450 4050 4650 4050
Connection ~ 3350 3950
Wire Wire Line
	3550 3950 4650 3950
Text HLabel 4200 4250 0    60   Input ~ 0
SCK
Wire Wire Line
	4200 4250 4650 4250
Wire Wire Line
	4200 4350 4650 4350
Wire Wire Line
	7750 4150 7550 4150
Wire Wire Line
	7750 4050 7550 4050
$Comp
L Device:LED D1
U 1 1 6117F8D9
P 8250 3400
F 0 "D1" V 8289 3282 50  0000 R CNN
F 1 "LED" V 8198 3282 50  0000 R CNN
F 2 "LED_SMD:LED_0603_1608Metric_Pad1.05x0.95mm_HandSolder" H 8250 3400 50  0001 C CNN
F 3 "~" H 8250 3400 50  0001 C CNN
	1    8250 3400
	0    -1   -1   0   
$EndComp
Connection ~ 6000 4850
$Comp
L Library-cbas:PIC16F19156 PIC16F19156-xMV1
U 1 1 610FEF38
P 6150 3950
F 0 "PIC16F19156-xMV1" H 6150 5100 50  0000 C CNN
F 1 "PIC16F19156" H 6150 3950 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-28-1EP_4x4mm_P0.4mm_EP2.6x2.6mm" H 6700 4100 50  0001 C CNN
F 3 "" H 5600 4000 50  0001 C CNN
	1    6150 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 3250 8250 3250
Text HLabel 8250 3700 2    60   Input ~ 0
GNDD
Wire Wire Line
	8250 3550 8250 3700
Wire Wire Line
	7550 4450 8500 4450
Wire Wire Line
	7550 4350 8500 4350
$EndSCHEMATC
