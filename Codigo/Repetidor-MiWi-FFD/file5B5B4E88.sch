EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip
LIBS:REGL
LIBS:FFD-PAN-Rep-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 6000 2700 2    60   Input ~ 0
VCCP
Text HLabel 5550 2350 0    60   Input ~ 0
VCCD
Text HLabel 4350 3900 0    60   Input ~ 0
VPP
Text HLabel 6850 3800 2    60   Input ~ 0
PGC
Text HLabel 6850 3900 2    60   Input ~ 0
PGD
Text HLabel 6050 5100 2    60   Input ~ 0
GNDD
$Comp
L R R6
U 1 1 5B5B5832
P 4450 3600
F 0 "R6" V 4550 3600 50  0000 C CNN
F 1 "R" V 4450 3600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 4380 3600 50  0001 C CNN
F 3 "" H 4450 3600 50  0001 C CNN
	1    4450 3600
	-1   0    0    1   
$EndComp
Text HLabel 4350 3300 0    60   Input ~ 0
VCCD
Text HLabel 7650 3600 2    60   Output ~ 0
SCK
Text HLabel 6850 3700 2    60   Input ~ 0
SDI
Text HLabel 7650 4800 2    60   Output ~ 0
SDO
Text HLabel 6850 4500 2    60   Output ~ 0
CS
$Comp
L R R5
U 1 1 5B5B5833
P 3150 4400
F 0 "R5" V 3250 4400 50  0000 C CNN
F 1 "R" V 3150 4400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 3080 4400 50  0001 C CNN
F 3 "" H 3150 4400 50  0001 C CNN
	1    3150 4400
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5B5B5834
P 2800 4400
F 0 "R4" V 2900 4400 50  0000 C CNN
F 1 "R" V 2800 4400 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 2730 4400 50  0001 C CNN
F 3 "" H 2800 4400 50  0001 C CNN
	1    2800 4400
	1    0    0    -1  
$EndComp
Text HLabel 2800 3900 2    60   Input ~ 0
VCCD
Text HLabel 7500 3300 2    60   Input ~ 0
SDA-i2c
Text HLabel 7500 3400 2    60   Input ~ 0
SDO-i2c
Text HLabel 3150 4650 2    60   Input ~ 0
SDA-i2c
Text HLabel 2800 4800 2    60   Input ~ 0
SDO-i2c
Text Notes 7950 3400 0    60   ~ 0
I2C-SOFT
Text HLabel 4950 3200 0    60   Input ~ 0
AN0
Text HLabel 4950 3300 0    60   Input ~ 0
AN1
Text HLabel 4950 3400 0    60   Input ~ 0
AN2
Text HLabel 4950 3500 0    60   Input ~ 0
AN3
Text HLabel 4950 3600 0    60   Input ~ 0
AN4
Text HLabel 4950 3700 0    60   Input ~ 0
AN5
Text HLabel 6850 3200 2    60   Input ~ 0
INT0
Text HLabel 6850 3400 2    60   Input ~ 0
INT3/2
Text HLabel 6850 3300 2    60   Input ~ 0
INT1
Text HLabel 6850 3500 2    60   Input ~ 0
AN9
Text HLabel 4950 4100 0    60   Input ~ 0
AN6
Text HLabel 4950 4200 0    60   Input ~ 0
AN7
Text Notes 2600 3750 0    60   ~ 0
I2C PULL-UPPS
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
NoConn ~ 4950 3300
NoConn ~ 4950 3400
NoConn ~ 4950 3500
NoConn ~ 4950 3600
NoConn ~ 4950 3700
NoConn ~ 4950 4100
NoConn ~ 4950 4200
$Comp
L C_Small C6
U 1 1 5B5B5835
P 2700 2650
F 0 "C6" H 2710 2720 50  0000 L CNN
F 1 "C_Small" H 2710 2570 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 2700 2650 50  0001 C CNN
F 3 "" H 2700 2650 50  0001 C CNN
	1    2700 2650
	1    0    0    -1  
$EndComp
$Comp
L CP_Small C7
U 1 1 5B5B5836
P 3100 2650
F 0 "C7" H 3110 2720 50  0000 L CNN
F 1 "CP_Small" H 3110 2570 50  0000 L CNN
F 2 "Capacitors_SMD:C_1206_HandSoldering" H 3100 2650 50  0001 C CNN
F 3 "" H 3100 2650 50  0001 C CNN
	1    3100 2650
	1    0    0    -1  
$EndComp
Text HLabel 3100 2950 2    60   Input ~ 0
GNDD
Text HLabel 3100 2350 2    60   Input ~ 0
VCCP
$Comp
L PIC18F26J50_I/SP U3
U 1 1 5B5B5837
P 5900 4000
F 0 "U3" H 5200 4950 50  0000 L CNN
F 1 "PIC18F26J50_I/SP" H 6600 4950 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm_Socket_LongPads" H 5800 5100 100 0001 C CNN
F 3 "" H 5900 3950 50  0001 C CNN
	1    5900 4000
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4400 6850 4400
Wire Wire Line
	6700 4300 6850 4300
Wire Wire Line
	5800 5100 6050 5100
Wire Wire Line
	4350 3900 5100 3900
Wire Wire Line
	4450 3750 4450 3900
Connection ~ 4450 3900
Wire Wire Line
	4350 3300 4450 3300
Wire Wire Line
	4450 3300 4450 3450
Wire Wire Line
	6700 3600 7650 3600
Wire Wire Line
	6850 4500 6700 4500
Wire Wire Line
	2800 3900 2800 4250
Wire Wire Line
	2800 4100 3150 4100
Wire Wire Line
	6700 3300 7500 3300
Wire Wire Line
	6700 3400 7500 3400
Wire Wire Line
	3150 4550 3150 4650
Wire Wire Line
	2800 4800 2800 4550
Wire Notes Line
	7450 3250 7450 3450
Wire Notes Line
	7450 3450 8400 3450
Wire Notes Line
	8400 3450 8400 3250
Wire Notes Line
	8400 3250 7450 3250
Wire Wire Line
	4600 3200 5100 3200
Wire Wire Line
	4950 3300 5100 3300
Wire Wire Line
	5100 3400 4950 3400
Wire Wire Line
	4950 3500 5100 3500
Wire Wire Line
	5100 3600 4950 3600
Wire Wire Line
	4950 3700 5100 3700
Wire Wire Line
	6850 3200 6700 3200
Wire Wire Line
	3150 4100 3150 4250
Connection ~ 2800 4100
Wire Notes Line
	2550 3800 2550 4900
Wire Notes Line
	2550 4900 3650 4900
Wire Notes Line
	3650 4900 3650 3800
Wire Notes Line
	3650 3800 2550 3800
Wire Wire Line
	6850 4100 6700 4100
Wire Wire Line
	6850 4200 6700 4200
Wire Wire Line
	4950 4100 5100 4100
Wire Wire Line
	4950 4200 5100 4200
Wire Wire Line
	5550 2350 5800 2350
Wire Wire Line
	5800 2350 5800 3000
Wire Wire Line
	5800 2700 6000 2700
Connection ~ 5800 2700
Connection ~ 5950 2700
Wire Wire Line
	2700 2550 3100 2550
Wire Wire Line
	2700 2750 3100 2750
Wire Wire Line
	3100 2550 3100 2350
Wire Wire Line
	3100 2750 3100 2950
Wire Wire Line
	6700 3900 6850 3900
Wire Wire Line
	5700 5000 5800 5000
Wire Wire Line
	5800 5000 5800 5100
Wire Wire Line
	6850 3500 6700 3500
NoConn ~ 6850 3500
Text HLabel 6850 4700 2    60   Input ~ 0
Wake
Text HLabel 6850 4600 2    60   Input ~ 0
!Reset
Wire Wire Line
	6850 4600 6700 4600
Wire Wire Line
	6850 4700 6700 4700
Wire Wire Line
	6850 3800 6700 3800
$Comp
L R R7
U 1 1 5B5B5838
P 7550 3750
F 0 "R7" V 7650 3750 50  0000 C CNN
F 1 "R" V 7550 3750 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7480 3750 50  0001 C CNN
F 3 "" H 7550 3750 50  0001 C CNN
	1    7550 3750
	-1   0    0    1   
$EndComp
$Comp
L R R8
U 1 1 5B5B5839
P 7550 4650
F 0 "R8" V 7650 4650 50  0000 C CNN
F 1 "R" V 7550 4650 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 7480 4650 50  0001 C CNN
F 3 "" H 7550 4650 50  0001 C CNN
	1    7550 4650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6700 4800 7650 4800
Wire Wire Line
	6700 3700 7300 3700
Connection ~ 7550 4800
Connection ~ 7550 3600
Text HLabel 7550 4000 2    60   Input ~ 0
VCCD
Text HLabel 7550 4400 2    60   Input ~ 0
VCCD
Wire Wire Line
	7550 4400 7550 4500
Wire Wire Line
	7550 3900 7550 4000
Text Notes 7750 3800 0    60   ~ 0
PULL-UPPs caso de alta impedancia asegura estado
Text Notes 7750 4650 0    60   ~ 0
PULL-UPPs caso de alta impedancia asegura estado
$Comp
L R R9
U 1 1 5B5B583A
P 8150 4100
F 0 "R9" V 8250 4100 50  0000 C CNN
F 1 "R" V 8150 4100 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P5.08mm_Horizontal" V 8080 4100 50  0001 C CNN
F 3 "" H 8150 4100 50  0001 C CNN
	1    8150 4100
	-1   0    0    1   
$EndComp
Wire Wire Line
	8150 4250 7300 4250
Wire Wire Line
	7300 4250 7300 3700
Text HLabel 8200 3900 2    60   Input ~ 0
VCCD
Wire Wire Line
	8200 3900 8150 3900
Wire Wire Line
	8150 3900 8150 3950
NoConn ~ 6850 4100
NoConn ~ 6850 4200
NoConn ~ 6850 4300
NoConn ~ 6850 4400
Text HLabel 4350 3050 0    60   Input ~ 0
Vbat
Wire Wire Line
	4350 3050 4600 3050
Wire Wire Line
	4600 3050 4600 3200
$EndSCHEMATC
