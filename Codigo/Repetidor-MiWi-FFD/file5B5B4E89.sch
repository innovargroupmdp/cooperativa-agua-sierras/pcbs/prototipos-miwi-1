EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:switches
LIBS:relays
LIBS:motors
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:microchip
LIBS:REGL
LIBS:FFD-PAN-Rep-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Conn_01x05_Male J1
U 1 1 5B5B5661
P 1800 4200
F 0 "J1" H 1800 4500 50  0000 C CNN
F 1 "Conn_01x05_Male" H 1800 3900 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_1x05_Pitch2.54mm" H 1800 4200 50  0001 C CNN
F 3 "" H 1800 4200 50  0001 C CNN
	1    1800 4200
	1    0    0    -1  
$EndComp
Text HLabel 2200 4000 2    60   Input ~ 0
VPP
Text HLabel 2200 4100 2    60   Input ~ 0
VCCP
Text HLabel 2200 4300 2    60   Input ~ 0
PGD
Text HLabel 2200 4400 2    60   Input ~ 0
PGC
Text HLabel 2200 4200 2    60   Input ~ 0
GNDD
Text Notes 1550 3850 0    60   ~ 0
ICSP
Wire Wire Line
	2000 4000 2200 4000
Wire Wire Line
	2200 4100 2000 4100
Wire Wire Line
	2000 4200 2200 4200
Wire Wire Line
	2200 4300 2000 4300
Wire Wire Line
	2200 4400 2000 4400
Wire Notes Line
	1550 4650 2650 4650
Wire Notes Line
	1550 3850 2650 3850
Wire Notes Line
	2650 3850 2650 4650
Wire Notes Line
	1550 3850 1550 4650
Text Notes 3550 3950 0    60   ~ 0
SPI-PORT2 (MEM)
Wire Notes Line
	3550 4600 4650 4600
Wire Notes Line
	3550 3950 4650 3950
Wire Notes Line
	4650 3950 4650 4600
Wire Notes Line
	3550 3950 3550 4600
Text Notes 3550 3150 0    60   ~ 0
SPI-PORT1 (MRF)
Wire Notes Line
	3550 3800 4650 3800
Wire Notes Line
	3550 3150 4650 3150
Wire Notes Line
	4650 3150 4650 3800
Wire Notes Line
	3550 3150 3550 3800
Text Notes 4900 3150 0    60   ~ 0
SPI-PORT3 (RF)
Wire Notes Line
	4900 3800 6000 3800
Wire Notes Line
	4900 3150 6000 3150
Wire Notes Line
	6000 3150 6000 3800
Wire Notes Line
	4900 3150 4900 3800
Text HLabel 3700 3700 2    60   Input ~ 0
SCK
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	6200 4650 6200 2800
Wire Notes Line
	6200 2800 3450 2800
Wire Notes Line
	3450 2800 3450 4650
Text Notes 4250 3050 0    51   ~ 0
BAHIAS DE EXPANSION SPI\nSE MONTAN VERTICALMENTE
Wire Notes Line
	10450 5500 6600 5500
Wire Notes Line
	6600 5500 6600 2800
Wire Notes Line
	6600 2800 10450 2800
Wire Notes Line
	10450 2800 10450 5500
Text Notes 7700 3150 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
Wire Notes Line
	3450 4650 6200 4650
Text HLabel 3700 3200 2    60   Input ~ 0
GNDD
Text HLabel 4350 3200 2    60   Input ~ 0
GNDD
Text HLabel 4350 3300 2    60   Input ~ 0
GNDD
Text HLabel 3700 3300 2    60   Input ~ 0
Reset
Text HLabel 3700 3400 2    60   Input ~ 0
Wake
Text HLabel 3700 3500 2    60   Input ~ 0
INT
Text HLabel 4350 3400 2    60   Input ~ 0
VCCD
Text HLabel 4350 3600 2    60   Input ~ 0
CS
Text HLabel 4350 3700 2    60   Input ~ 0
SDO
Text HLabel 3700 3600 2    60   Input ~ 0
SDI
NoConn ~ 4350 3500
$Comp
L Conn_01x06_Female J2
U 1 1 5B5B5662
P 3500 3400
F 0 "J2" H 3500 3700 50  0000 C CNN
F 1 "Conn_01x06_Female" H 3500 3000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 3500 3400 50  0001 C CNN
F 3 "" H 3500 3400 50  0001 C CNN
	1    3500 3400
	-1   0    0    -1  
$EndComp
$Comp
L Conn_01x06_Female J3
U 1 1 5B5B5663
P 4150 3400
F 0 "J3" H 4150 3700 50  0000 C CNN
F 1 "Conn_01x06_Female" H 4150 3000 50  0000 C CNN
F 2 "Socket_Strips:Socket_Strip_Straight_1x06_Pitch2.54mm" H 4150 3400 50  0001 C CNN
F 3 "" H 4150 3400 50  0001 C CNN
	1    4150 3400
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
