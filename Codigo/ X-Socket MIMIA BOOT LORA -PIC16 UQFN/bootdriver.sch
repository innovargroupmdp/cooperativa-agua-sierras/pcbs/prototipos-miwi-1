EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Library-cbas:PIC16F15214 U5
U 1 1 6136FE71
P 4450 3850
F 0 "U5" H 4450 4631 50  0000 C CNN
F 1 "PIC16F15214" H 4450 4540 50  0000 C CNN
F 2 "Package_DFN_QFN:DFN-8-1EP_3x3mm_P0.65mm_EP1.55x2.4mm" H 4450 3850 50  0001 C CNN
F 3 "" H 4450 3850 50  0001 C CNN
	1    4450 3850
	1    0    0    -1  
$EndComp
$Comp
L Device:D D3
U 1 1 613A6C6D
P 3450 4150
F 0 "D3" H 3450 3933 50  0000 C CNN
F 1 "D" H 3450 4024 50  0000 C CNN
F 2 "Diode_SMD:D_0402_1005Metric_Castellated" H 3450 4150 50  0001 C CNN
F 3 "~" H 3450 4150 50  0001 C CNN
	1    3450 4150
	-1   0    0    1   
$EndComp
Text HLabel 4450 3250 2    50   Input ~ 0
VCCD
Text HLabel 4450 4450 0    50   Input ~ 0
GNDD
Text HLabel 3300 3850 0    50   Input ~ 0
SCK
Text HLabel 3300 3750 0    50   Input ~ 0
MOSI
Wire Wire Line
	3600 3750 3850 3750
Text HLabel 6200 3850 0    50   Input ~ 0
MISO
Text HLabel 7400 4050 2    50   Input ~ 0
MOSI
$Comp
L Library-cbas:23k512 U6
U 1 1 61370137
P 6750 3700
F 0 "U6" H 6800 3875 50  0000 C CNN
F 1 "23k512" H 6800 3784 50  0000 C CNN
F 2 "Package_SO:TSSOP-8_4.4x3mm_P0.65mm" H 6800 3900 50  0001 C CNN
F 3 "22126E.pdf" H 6750 3700 50  0001 C CNN
	1    6750 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 3850 7100 3750
Wire Wire Line
	3600 3850 3800 3850
Text HLabel 7400 3750 2    50   Input ~ 0
VCCD
Text HLabel 7400 3950 2    50   Input ~ 0
SCK
Wire Wire Line
	7100 3950 7400 3950
Wire Wire Line
	7400 3750 7100 3750
Connection ~ 7100 3750
Wire Wire Line
	7400 4050 7100 4050
Wire Wire Line
	6200 3850 6500 3850
Wire Wire Line
	6200 4050 6500 4050
Text HLabel 6200 3750 0    50   Input ~ 0
nCS_SRAM
Wire Wire Line
	6200 3750 6500 3750
Text HLabel 6200 4050 0    50   Input ~ 0
GNDD
Text HLabel 5350 4000 2    50   Input ~ 0
MISO
Wire Wire Line
	5350 3900 5050 3900
Text HLabel 5350 3800 2    60   Input ~ 0
TX
Text HLabel 5350 3900 2    60   Input ~ 0
RX
Text HLabel 3300 4150 0    50   Input ~ 0
nCS_SRAM
$Comp
L Device:R R5
U 1 1 613C46F4
P 5200 4000
F 0 "R5" V 4993 4000 50  0000 C CNN
F 1 "R" V 5084 4000 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 5130 4000 50  0001 C CNN
F 3 "~" H 5200 4000 50  0001 C CNN
	1    5200 4000
	0    1    1    0   
$EndComp
Wire Wire Line
	3850 3950 3600 4150
Wire Wire Line
	5050 3800 5350 3800
$Comp
L Device:R R7
U 1 1 613B1F4B
P 3450 3850
F 0 "R7" V 3243 3850 50  0000 C CNN
F 1 "R" V 3334 3850 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 3380 3850 50  0001 C CNN
F 3 "~" H 3450 3850 50  0001 C CNN
	1    3450 3850
	0    1    1    0   
$EndComp
$Comp
L Device:R R6
U 1 1 613B21C4
P 3450 3750
F 0 "R6" V 3243 3750 50  0000 C CNN
F 1 "R" V 3334 3750 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 3380 3750 50  0001 C CNN
F 3 "~" H 3450 3750 50  0001 C CNN
	1    3450 3750
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x03_Male J4
U 1 1 613B5ED6
P 3650 2750
F 0 "J4" H 3758 3031 50  0000 C CNN
F 1 "Conn_01x03_Male" H 3758 2940 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x03_P1.27mm_Vertical" H 3650 2750 50  0001 C CNN
F 3 "~" H 3650 2750 50  0001 C CNN
	1    3650 2750
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3150 3850 3150
Wire Wire Line
	3850 3150 3850 3750
Connection ~ 3850 3750
Wire Wire Line
	3750 3250 3800 3250
Wire Wire Line
	3800 3250 3800 3850
Connection ~ 3800 3850
Wire Wire Line
	3800 3850 3850 3850
Text GLabel 3750 3150 0    50   Input ~ 0
ISCPDT
Text GLabel 3750 3250 0    50   Input ~ 0
ICSPCLK
Text GLabel 5050 4250 2    50   Input ~ 0
ICSPVPP
Wire Wire Line
	5050 4250 5050 4000
Text GLabel 3650 2850 2    50   Input ~ 0
ICSPVPP
Text GLabel 3650 2750 2    50   Input ~ 0
ISCPDT
Text GLabel 3650 2650 2    50   Input ~ 0
ICSPCLK
Wire Wire Line
	3650 2650 3450 2650
Wire Wire Line
	3650 2750 3450 2750
Wire Wire Line
	3650 2850 3450 2850
Connection ~ 5050 4000
$EndSCHEMATC
