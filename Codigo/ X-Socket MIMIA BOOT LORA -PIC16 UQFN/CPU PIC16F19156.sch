EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 5 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text HLabel 5300 2550 0    63   Input ~ 0
VCCD
Text HLabel 4200 3050 0    63   Input ~ 0
VPP
Text HLabel 6200 5050 2    60   Input ~ 0
GNDD
Text HLabel 4200 4350 0    63   Input ~ 0
RC4-ANC4
Text HLabel 7850 4150 2    63   Input ~ 0
RB2-ANB2
Text Notes 3700 750  0    118  ~ 0
MICROCONTROLADOR Y LOGICA DE CONTROL DE BUS
$Comp
L Monitor-Caudal-rescue:C_Small C1
U 1 1 5B5B5835
P 2400 2000
F 0 "C1" H 2410 2070 50  0000 L CNN
F 1 "C_Small" H 2410 1920 50  0000 L CNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2400 2000 50  0001 C CNN
F 3 "" H 2400 2000 50  0001 C CNN
	1    2400 2000
	1    0    0    -1  
$EndComp
Text HLabel 2400 2300 2    60   Input ~ 0
GNDD
Text HLabel 7850 4050 2    63   Input ~ 0
RB1-ANB1
Text HLabel 7850 3750 2    63   Input ~ 0
RA7-ANA7
Wire Wire Line
	5300 2550 5550 2550
Wire Wire Line
	2400 1900 2400 1700
Wire Wire Line
	2400 2100 2400 2300
Wire Wire Line
	7850 3750 7550 3750
Wire Wire Line
	5550 2550 5550 2850
Wire Wire Line
	6000 5050 6200 5050
Wire Wire Line
	5900 4850 6000 4850
Wire Wire Line
	6000 4850 6000 5050
NoConn ~ 6100 4850
Text HLabel 4200 4650 0    63   Output ~ 0
RC7-ANC7
Wire Wire Line
	4650 4650 4200 4650
$Comp
L Device:Crystal Y1
U 1 1 61117879
P 3100 3950
F 0 "Y1" H 3100 4218 50  0000 C CNN
F 1 "Crystal" H 3100 4127 50  0000 C CNN
F 2 "Crystal:Crystal_SMD_3215-2Pin_3.2x1.5mm" H 3100 3950 50  0001 C CNN
F 3 "~" H 3100 3950 50  0001 C CNN
	1    3100 3950
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:C_Small C2
U 1 1 61127649
P 2800 4100
F 0 "C2" H 2810 4170 50  0000 L CNN
F 1 "C_Small" H 2810 4020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric_Pad0.74x0.62mm_HandSolder" H 2800 4100 50  0001 C CNN
F 3 "" H 2800 4100 50  0001 C CNN
	1    2800 4100
	1    0    0    -1  
$EndComp
$Comp
L Monitor-Caudal-rescue:C_Small C6
U 1 1 61127AED
P 3350 4100
F 0 "C6" H 3360 4170 50  0000 L CNN
F 1 "C_Small" H 3360 4020 50  0000 L CNN
F 2 "Capacitor_SMD:C_0402_1005Metric_Pad0.74x0.62mm_HandSolder" H 3350 4100 50  0001 C CNN
F 3 "" H 3350 4100 50  0001 C CNN
	1    3350 4100
	1    0    0    -1  
$EndComp
Text HLabel 2800 4300 0    60   Input ~ 0
GNDD
Text HLabel 3350 4300 0    60   Input ~ 0
GNDD
Wire Wire Line
	2800 4200 2800 4300
Wire Wire Line
	3350 4200 3350 4300
Wire Wire Line
	3350 4000 3350 3950
Wire Wire Line
	3350 3950 3250 3950
Wire Wire Line
	2950 3950 2800 3950
Wire Wire Line
	2800 3950 2800 4000
Wire Wire Line
	2800 3950 2800 3800
Wire Wire Line
	2800 3800 3550 3800
Wire Wire Line
	3550 3800 3550 3950
Connection ~ 2800 3950
Wire Wire Line
	3450 4050 3450 3950
Wire Wire Line
	3450 3950 3350 3950
Wire Wire Line
	3450 4050 4650 4050
Connection ~ 3350 3950
Wire Wire Line
	3550 3950 4650 3950
Text HLabel 4200 4250 0    63   Input ~ 0
RC3-ANC3
Wire Wire Line
	4200 4250 4650 4250
Wire Wire Line
	4200 4350 4650 4350
Text HLabel 7850 3950 2    63   Output ~ 0
RB0-ANB0
Text HLabel 7850 3650 2    63   Output ~ 0
RA6-ANA6
Wire Wire Line
	7850 3550 7550 3550
Wire Wire Line
	7850 3650 7550 3650
Text HLabel 7850 4250 2    63   Input ~ 0
RB3-ANB3
Text HLabel 7850 4350 2    63   Input ~ 0
RB4-ANB4
Wire Wire Line
	7850 4350 7550 4350
Wire Wire Line
	4300 3050 4650 3050
Wire Wire Line
	4200 3050 4300 3050
Connection ~ 4300 3050
Wire Wire Line
	4300 2950 4300 3050
Wire Wire Line
	4200 2550 4300 2550
Text HLabel 4200 2550 0    63   Input ~ 0
VCCD
Wire Wire Line
	4300 2550 4300 2650
$Comp
L Monitor-Caudal-rescue:R R2
U 1 1 5B5B5832
P 4300 2800
F 0 "R2" V 4400 2800 50  0000 C CNN
F 1 "R" V 4300 2800 50  0000 C CNN
F 2 "Resistor_SMD:R_0402_1005Metric_Pad0.72x0.64mm_HandSolder" V 4230 2800 50  0001 C CNN
F 3 "" H 4300 2800 50  0001 C CNN
	1    4300 2800
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 3350 7550 3350
Text HLabel 7850 3050 2    63   Input ~ 0
RA0-ANA0
Text Notes 1000 1600 0    60   ~ 0
ICSP
Wire Notes Line
	1000 2400 2100 2400
Wire Notes Line
	1000 1600 2100 1600
Wire Notes Line
	2100 1600 2100 2400
Wire Notes Line
	1000 1600 1000 2400
Wire Wire Line
	4650 4150 4200 4150
Wire Wire Line
	7850 3250 7550 3250
Text HLabel 7850 3250 2    63   Input ~ 0
RA2-DAC1
Text HLabel 7850 3350 2    63   Input ~ 0
Vref+
Text HLabel 8150 3150 2    63   Input ~ 0
RA1-ANA1
Text HLabel 7850 4650 2    63   Input ~ 0
RB7-ANB7-DAC2-RX2-ISPDT
Text HLabel 7850 4550 2    63   Input ~ 0
RB6-ANB6-TX2-ISPCLK
Text HLabel 4200 4150 0    63   Input ~ 0
RC2-ANC2
Text HLabel 4200 4550 0    63   Input ~ 0
RC6-ANC6
Wire Wire Line
	4200 4550 4650 4550
Text HLabel 7850 4450 2    63   Input ~ 0
RB5-ANB5
Wire Wire Line
	7850 4450 7550 4450
Text HLabel 2400 1700 2    63   Input ~ 0
VCCD
Wire Wire Line
	7550 4550 7850 4550
Wire Wire Line
	7550 4650 7850 4650
Connection ~ 6000 4850
$Comp
L Library-cbas:PIC16F19156 PIC16F19156-xMV1
U 1 1 610FEF38
P 6150 3950
F 0 "PIC16F19156-xMV1" H 6150 5100 50  0000 C CNN
F 1 "PIC16F19156" H 6150 3950 50  0000 C CNN
F 2 "Package_DFN_QFN:QFN-28-1EP_4x4mm_P0.4mm_EP2.6x2.6mm" H 6700 4100 50  0001 C CNN
F 3 "" H 5600 4000 50  0001 C CNN
	1    6150 3950
	1    0    0    -1  
$EndComp
Text HLabel 7850 3550 2    63   Input ~ 0
Vbat
Wire Wire Line
	7850 3950 7550 3950
$Comp
L Library-cbas:25AA02E-OT U1
U 1 1 61421596
P 4100 5900
F 0 "U1" H 4100 6381 50  0000 C CNN
F 1 "25AA02E-OT" H 4100 6290 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-6" H 4100 6400 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/21709J.pdf" H 4100 5900 50  0001 C CNN
	1    4100 5900
	1    0    0    -1  
$EndComp
Text GLabel 4750 6000 2    59   Input ~ 0
nCS_EE
Text GLabel 4750 5900 2    59   Input ~ 0
Sck
Text GLabel 4750 5800 2    59   Input ~ 0
MOSI
Text GLabel 3550 5900 0    59   Input ~ 0
MISO
Wire Wire Line
	3550 5900 3700 5900
Wire Wire Line
	4500 5800 4750 5800
Wire Wire Line
	4750 5900 4500 5900
Wire Wire Line
	4500 6000 4750 6000
Text HLabel 4100 5600 0    63   Input ~ 0
VCCD
Text HLabel 4100 6200 2    60   Input ~ 0
GNDD
Text GLabel 8750 4150 2    59   Input ~ 0
MISO
Text GLabel 8750 4050 2    59   Input ~ 0
MOSI
Text GLabel 8750 4250 2    59   Input ~ 0
Sck
Wire Wire Line
	7550 4250 8750 4250
Wire Wire Line
	7550 4150 8750 4150
Wire Wire Line
	7550 4050 8750 4050
$Comp
L Device:D D1
U 1 1 613ECE80
P 8000 3150
F 0 "D1" H 8000 3350 50  0000 C CNN
F 1 "D" H 8000 3276 50  0000 C CNN
F 2 "Diode_SMD:D_0402_1005Metric_Castellated" H 8000 3150 50  0001 C CNN
F 3 "~" H 8000 3150 50  0001 C CNN
	1    8000 3150
	1    0    0    -1  
$EndComp
Text GLabel 7850 3450 2    59   Input ~ 0
nCS_EE
Wire Wire Line
	7550 3150 7850 3150
Wire Wire Line
	7850 3050 7550 3050
Wire Wire Line
	7850 3450 7550 3450
$EndSCHEMATC
