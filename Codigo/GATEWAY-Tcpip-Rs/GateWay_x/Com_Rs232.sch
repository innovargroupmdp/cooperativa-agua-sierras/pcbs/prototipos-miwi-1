EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 9 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gateway-rescue:MAX232 U?
U 1 1 5B4D4792
P 5300 3300
F 0 "U?" H 5200 4425 50  0000 R CNN
F 1 "MAX232" H 5200 4350 50  0000 R CNN
F 2 "" H 5350 2250 50  0001 L CNN
F 3 "" H 5300 3400 50  0001 C CNN
	1    5300 3300
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:CP_Small C?
U 1 1 5B4D47CA
P 4350 2550
F 0 "C?" H 4360 2620 50  0000 L CNN
F 1 "CP_Small" H 4360 2470 50  0000 L CNN
F 2 "" H 4350 2550 50  0001 C CNN
F 3 "" H 4350 2550 50  0001 C CNN
	1    4350 2550
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:CP_Small C?
U 1 1 5B4D47CB
P 6300 2550
F 0 "C?" H 6310 2620 50  0000 L CNN
F 1 "CP_Small" H 6310 2470 50  0000 L CNN
F 2 "" H 6300 2550 50  0001 C CNN
F 3 "" H 6300 2550 50  0001 C CNN
	1    6300 2550
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:CP_Small C?
U 1 1 5B4D47CC
P 4350 2200
F 0 "C?" H 4360 2270 50  0000 L CNN
F 1 "CP_Small" H 4360 2120 50  0000 L CNN
F 2 "" H 4350 2200 50  0001 C CNN
F 3 "" H 4350 2200 50  0001 C CNN
	1    4350 2200
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:CP_Small C?
U 1 1 5B4D47CD
P 6400 3000
F 0 "C?" H 6410 3070 50  0000 L CNN
F 1 "CP_Small" H 6410 2920 50  0000 L CNN
F 2 "" H 6400 3000 50  0001 C CNN
F 3 "" H 6400 3000 50  0001 C CNN
	1    6400 3000
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:CP_Small C?
U 1 1 5B4D47CE
P 6400 3300
F 0 "C?" H 6410 3370 50  0000 L CNN
F 1 "CP_Small" H 6410 3220 50  0000 L CNN
F 2 "" H 6400 3300 50  0001 C CNN
F 3 "" H 6400 3300 50  0001 C CNN
	1    6400 3300
	-1   0    0    1   
$EndComp
Wire Wire Line
	4500 2400 4350 2400
Wire Wire Line
	4350 2400 4350 2450
Wire Wire Line
	4500 2700 4350 2700
Wire Wire Line
	4350 2700 4350 2650
Wire Wire Line
	6100 2400 6300 2400
Wire Wire Line
	6300 2400 6300 2450
Wire Wire Line
	6300 2650 6300 2700
Wire Wire Line
	6300 2700 6100 2700
Wire Wire Line
	6000 2900 6400 2900
Wire Wire Line
	6100 3200 6400 3200
Wire Wire Line
	4350 2100 5300 2100
Wire Wire Line
	6100 3800 6750 3800
Wire Wire Line
	6100 3400 6250 3400
Wire Wire Line
	6250 3400 6250 3900
Wire Wire Line
	6250 3900 6750 3900
$Comp
L power1:GNDD #PWR?
U 1 1 5B4D47CF
P 6400 3100
F 0 "#PWR?" H 6400 2850 50  0001 C CNN
F 1 "GNDD" H 6400 2975 50  0000 C CNN
F 2 "" H 6400 3100 50  0001 C CNN
F 3 "" H 6400 3100 50  0001 C CNN
	1    6400 3100
	1    0    0    -1  
$EndComp
$Comp
L power1:GNDD #PWR?
U 1 1 5B4D47D0
P 6400 3400
F 0 "#PWR?" H 6400 3150 50  0001 C CNN
F 1 "GNDD" H 6400 3275 50  0000 C CNN
F 2 "" H 6400 3400 50  0001 C CNN
F 3 "" H 6400 3400 50  0001 C CNN
	1    6400 3400
	1    0    0    -1  
$EndComp
$Comp
L power1:GNDD #PWR?
U 1 1 5B4D47D1
P 5300 4500
F 0 "#PWR?" H 5300 4250 50  0001 C CNN
F 1 "GNDD" H 5300 4375 50  0000 C CNN
F 2 "" H 5300 4500 50  0001 C CNN
F 3 "" H 5300 4500 50  0001 C CNN
	1    5300 4500
	1    0    0    -1  
$EndComp
$Comp
L power1:GNDD #PWR?
U 1 1 5B4D47D2
P 4350 2300
F 0 "#PWR?" H 4350 2050 50  0001 C CNN
F 1 "GNDD" H 4350 2175 50  0000 C CNN
F 2 "" H 4350 2300 50  0001 C CNN
F 3 "" H 4350 2300 50  0001 C CNN
	1    4350 2300
	1    0    0    -1  
$EndComp
$Comp
L power1:+5V #PWR?
U 1 1 5B4D47D3
P 5300 2100
F 0 "#PWR?" H 5300 1950 50  0001 C CNN
F 1 "+5V" H 5300 2240 50  0000 C CNN
F 2 "" H 5300 2100 50  0001 C CNN
F 3 "" H 5300 2100 50  0001 C CNN
	1    5300 2100
	1    0    0    -1  
$EndComp
$Comp
L power1:+5V #PWR?
U 1 1 5B4D47D5
P 6600 3500
F 0 "#PWR?" H 6600 3350 50  0001 C CNN
F 1 "+5V" H 6600 3640 50  0000 C CNN
F 2 "" H 6600 3500 50  0001 C CNN
F 3 "" H 6600 3500 50  0001 C CNN
	1    6600 3500
	1    0    0    -1  
$EndComp
$Comp
L power1:GNDD #PWR?
U 1 1 5B4D47D6
P 6600 4150
F 0 "#PWR?" H 6600 3900 50  0001 C CNN
F 1 "GNDD" H 6600 4025 50  0000 C CNN
F 2 "" H 6600 4150 50  0001 C CNN
F 3 "" H 6600 4150 50  0001 C CNN
	1    6600 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6750 4000 6600 4000
Wire Wire Line
	6600 4000 6600 4150
Wire Wire Line
	6750 3700 6600 3700
Wire Wire Line
	6600 3700 6600 3500
Wire Wire Line
	6750 4100 6600 4100
Connection ~ 6600 4100
$EndSCHEMATC
