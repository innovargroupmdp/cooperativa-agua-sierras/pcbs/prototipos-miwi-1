EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1700 6250 500  400 
U 5B523CE1
F0 "Fuente_swicht_485_1" 60
F1 "Fuente_swicht_485_1.sch" 60
$EndSheet
$Sheet
S 1700 7050 500  400 
U 5B523D76
F0 "Fuente_swicht_485_2" 60
F1 "Fuente_swicht_485_2.sch" 60
$EndSheet
$Sheet
S 1700 5250 1550 600 
U 5B523DC2
F0 "Fuente_gral" 60
F1 "Fuente_gral.sch" 60
$EndSheet
$Sheet
S 7550 2900 500  750 
U 5B524BD1
F0 "Com_Rs_485_1" 60
F1 "Com_Rs_485_1.sch" 60
$EndSheet
$Sheet
S 7550 1850 500  750 
U 5B524C1D
F0 "Com_Rs_485_2" 60
F1 "Com_Rs_485_2.sch" 60
$EndSheet
$Sheet
S 5400 1850 1300 2800
U 5B52521D
F0 "CPU PIC18F26K20" 60
F1 "CPU.sch" 60
$EndSheet
$Sheet
S 3900 1850 600  2850
U 5B4CDC50
F0 "Puertos_IO" 60
F1 "Puertos_IO.sch" 60
$EndSheet
$Sheet
S 7550 3900 500  750 
U 5B4D3CE6
F0 "Com_Rs232" 60
F1 "Com_Rs232.sch" 60
$EndSheet
$Comp
L gateway-rescue:RJ12 J?
U 1 1 5B4E9749
P 1950 3250
F 0 "J?" H 2150 3750 50  0000 C CNN
F 1 "RJ12" H 1800 3750 50  0000 C CNN
F 2 "" H 1950 3250 50  0001 C CNN
F 3 "" H 1950 3250 50  0001 C CNN
	1    1950 3250
	0    -1   1    0   
$EndComp
$Comp
L gateway-rescue:RJ12 J?
U 1 1 5B4EA1E9
P 1950 4350
F 0 "J?" H 2150 4850 50  0000 C CNN
F 1 "RJ12" H 1800 4850 50  0000 C CNN
F 2 "" H 1950 4350 50  0001 C CNN
F 3 "" H 1950 4350 50  0001 C CNN
	1    1950 4350
	0    -1   1    0   
$EndComp
$Comp
L gateway-rescue:RJ12 J?
U 1 1 5B4EA56D
P 1950 2100
F 0 "J?" H 2150 2600 50  0000 C CNN
F 1 "RJ12" H 1800 2600 50  0000 C CNN
F 2 "" H 1950 2100 50  0001 C CNN
F 3 "" H 1950 2100 50  0001 C CNN
	1    1950 2100
	0    -1   1    0   
$EndComp
$EndSCHEMATC
