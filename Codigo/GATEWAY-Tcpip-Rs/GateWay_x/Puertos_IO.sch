EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 9
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L gateway-rescue:Conn_02x05_Odd_Even J?
U 1 1 5B4EB63F
P 1250 5050
F 0 "J?" H 1300 5350 50  0000 C CNN
F 1 "Conn_02x05_Odd_Even" H 1300 4750 50  0000 C CNN
F 2 "" H 1250 5050 50  0001 C CNN
F 3 "" H 1250 5050 50  0001 C CNN
	1    1250 5050
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x06_Female J?
U 1 1 5B4EB640
P 10450 2300
F 0 "J?" H 10450 2600 50  0000 C CNN
F 1 "Conn_01x06_Female" H 10450 1900 50  0000 C CNN
F 2 "" H 10450 2300 50  0001 C CNN
F 3 "" H 10450 2300 50  0001 C CNN
	1    10450 2300
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x06_Female J?
U 1 1 5B4EB641
P 10450 4050
F 0 "J?" H 10450 4350 50  0000 C CNN
F 1 "Conn_01x06_Female" H 10450 3700 50  0000 C CNN
F 2 "" H 10450 4050 50  0001 C CNN
F 3 "" H 10450 4050 50  0001 C CNN
	1    10450 4050
	1    0    0    -1  
$EndComp
Text Notes 9850 1400 0    60   ~ 0
I/O RS485-1 MODULO: \nAntena
Text Notes 9850 3150 0    60   ~ 0
I/O RS485-2 MODULO:\nRed interna
$Comp
L gateway-rescue:Conn_01x04_Female J?
U 1 1 5B4EB642
P 8600 4050
F 0 "J?" H 8600 4250 50  0000 C CNN
F 1 "Conn_01x04_Female" H 8600 3750 50  0000 C CNN
F 2 "" H 8600 4050 50  0001 C CNN
F 3 "" H 8600 4050 50  0001 C CNN
	1    8600 4050
	1    0    0    -1  
$EndComp
Text Notes 8000 3300 0    60   ~ 0
I/O RS232 MODULO:\nGSM-GPS-OTROS
Text HLabel 1000 4850 0    60   Input ~ 0
clkout
Text HLabel 1000 4950 0    60   Input ~ 0
wol
Text HLabel 1000 5050 0    60   Input ~ 0
SDI
Text HLabel 1000 5150 0    60   Input ~ 0
CS
Text HLabel 1000 5250 0    60   Input ~ 0
VCCD
Text HLabel 1600 4850 2    60   Input ~ 0
INT
Text HLabel 1600 4950 2    60   Input ~ 0
SDO
Text HLabel 1600 5050 2    60   Input ~ 0
SCK
Text HLabel 1600 5150 2    60   Input ~ 0
RESET
Text HLabel 1600 5250 2    60   Input ~ 0
GNDD
Text HLabel 9700 1450 0    60   Input ~ 0
VCC1
Text HLabel 9700 3250 0    60   Input ~ 0
VCC2
Text HLabel 9700 3550 0    60   Input ~ 0
GND2
$Comp
L gateway-rescue:Conn_01x04_Female J?
U 1 1 5B4EB643
P 10450 1550
F 0 "J?" H 10450 1750 50  0000 C CNN
F 1 "Conn_01x04_Female" H 10450 1250 50  0000 C CNN
F 2 "" H 10450 1550 50  0001 C CNN
F 3 "" H 10450 1550 50  0001 C CNN
	1    10450 1550
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x04_Female J?
U 1 1 5B4EB644
P 10450 3350
F 0 "J?" H 10450 3550 50  0000 C CNN
F 1 "Conn_01x04_Female" H 10450 3050 50  0000 C CNN
F 2 "" H 10450 3350 50  0001 C CNN
F 3 "" H 10450 3350 50  0001 C CNN
	1    10450 3350
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x04_Female J?
U 1 1 5B4EB645
P 8600 3500
F 0 "J?" H 8600 3700 50  0000 C CNN
F 1 "Conn_01x04_Female" H 8600 3200 50  0000 C CNN
F 2 "" H 8600 3500 50  0001 C CNN
F 3 "" H 8600 3500 50  0001 C CNN
	1    8600 3500
	1    0    0    -1  
$EndComp
Text HLabel 9700 2100 0    60   Input ~ 0
VCCD
Text HLabel 9700 2200 0    60   Input ~ 0
GNDD
Text HLabel 7850 3950 0    60   Input ~ 0
VCCD
Text HLabel 7850 4050 0    60   Input ~ 0
GNDD
Text HLabel 9700 3850 0    60   Input ~ 0
VCCD
Text HLabel 9700 3950 0    60   Input ~ 0
GNDD
Text HLabel 9700 1550 0    60   Input ~ 0
RS485-B1
Text HLabel 9700 1650 0    60   Input ~ 0
RS485-A1
Text HLabel 9700 3350 0    60   Input ~ 0
RS485-B2
Text HLabel 9700 3450 0    60   Input ~ 0
RS485-A2
Text HLabel 7850 3400 0    60   Input ~ 0
VCCD
Text HLabel 7850 3500 0    60   Input ~ 0
GNDD
Text HLabel 7850 3600 0    60   Input ~ 0
RS232IN
$Comp
L gateway-rescue:Conn_01x05_Male J?
U 1 1 5B4EB646
P 850 6050
F 0 "J?" H 850 6350 50  0000 C CNN
F 1 "Conn_01x05_Male" H 850 5750 50  0000 C CNN
F 2 "" H 850 6050 50  0001 C CNN
F 3 "" H 850 6050 50  0001 C CNN
	1    850  6050
	1    0    0    -1  
$EndComp
Text HLabel 1250 5850 2    60   Input ~ 0
VPP
Text HLabel 1250 5950 2    60   Input ~ 0
VCCP
Text HLabel 1250 6150 2    60   Input ~ 0
PGD
Text HLabel 1250 6250 2    60   Input ~ 0
PGC
Text HLabel 1250 6050 2    60   Input ~ 0
GNDD
Text HLabel 1350 4150 2    60   Input ~ 0
SDA-i2c
Text HLabel 1350 4250 2    60   Input ~ 0
SDO-i2c
$Comp
L gateway-rescue:Conn_01x04_Male J?
U 1 1 5B4EB64E
P 950 4050
F 0 "J?" H 950 4350 50  0000 C CNN
F 1 "Conn_01x04_Male" H 950 3750 50  0000 C CNN
F 2 "" H 950 4050 50  0001 C CNN
F 3 "" H 950 4050 50  0001 C CNN
	1    950  4050
	1    0    0    -1  
$EndComp
Text HLabel 1350 3950 2    60   Input ~ 0
VCCD
Text HLabel 1350 4050 2    60   Input ~ 0
GNDD
$Comp
L gateway-rescue:Conn_01x08_Male J?
U 1 1 5B4EB64F
P 950 1200
F 0 "J?" H 950 1500 50  0000 C CNN
F 1 "Conn_01x08_Male" H 950 750 50  0000 C CNN
F 2 "" H 950 1200 50  0001 C CNN
F 3 "" H 950 1200 50  0001 C CNN
	1    950  1200
	1    0    0    -1  
$EndComp
Text HLabel 1350 900  2    60   Input ~ 0
P1
Text HLabel 1350 1000 2    60   Input ~ 0
P2
Text HLabel 1350 1100 2    60   Input ~ 0
P3
Text HLabel 1350 1200 2    60   Input ~ 0
P4
Text HLabel 1350 1300 2    60   Input ~ 0
P5
Text HLabel 1350 1500 2    60   Input ~ 0
P7
Text HLabel 1350 1400 2    60   Input ~ 0
P6
Text HLabel 1350 1600 2    60   Input ~ 0
P8
Text HLabel 1350 2050 2    60   Input ~ 0
SDA-i2c
Text HLabel 1350 2150 2    60   Input ~ 0
SDO-i2c
$Comp
L gateway-rescue:Conn_01x05_Male J?
U 1 1 5B4EB650
P 950 1950
F 0 "J?" H 950 2250 50  0000 C CNN
F 1 "Conn_01x05_Male" H 950 1650 50  0000 C CNN
F 2 "" H 950 1950 50  0001 C CNN
F 3 "" H 950 1950 50  0001 C CNN
	1    950  1950
	1    0    0    -1  
$EndComp
Text HLabel 1350 1850 2    60   Input ~ 0
VCCD
Text HLabel 1350 1950 2    60   Input ~ 0
GNDD
$Comp
L gateway-rescue:Conn_01x08_Male J?
U 1 1 5B4EB651
P 2600 1200
F 0 "J?" H 2600 1500 50  0000 C CNN
F 1 "Conn_01x08_Male" H 2600 750 50  0000 C CNN
F 2 "" H 2600 1200 50  0001 C CNN
F 3 "" H 2600 1200 50  0001 C CNN
	1    2600 1200
	1    0    0    -1  
$EndComp
Text HLabel 3000 900  2    60   Input ~ 0
P9
Text HLabel 3000 1000 2    60   Input ~ 0
P10
Text HLabel 3000 1100 2    60   Input ~ 0
P11
Text HLabel 3000 1200 2    60   Input ~ 0
P12
Text HLabel 3000 1300 2    60   Input ~ 0
P13
Text HLabel 3000 1500 2    60   Input ~ 0
P15
Text HLabel 3000 1400 2    60   Input ~ 0
P14
Text HLabel 3000 1600 2    60   Input ~ 0
P16
Text Notes 700  3700 0    60   ~ 0
SALIDA LCD ALFA-NUMERICO
Text Notes 600  2750 0    60   ~ 0
SALIDA GLCD
Text HLabel 1350 3200 2    60   Input ~ 0
SDA-i2c
Text HLabel 1350 3300 2    60   Input ~ 0
SDO-i2c
$Comp
L gateway-rescue:Conn_01x04_Male J?
U 1 1 5B4EB652
P 950 3100
F 0 "J?" H 950 3400 50  0000 C CNN
F 1 "Conn_01x04_Male" H 950 2800 50  0000 C CNN
F 2 "" H 950 3100 50  0001 C CNN
F 3 "" H 950 3100 50  0001 C CNN
	1    950  3100
	1    0    0    -1  
$EndComp
Text HLabel 1350 3000 2    60   Input ~ 0
VCCD
Text HLabel 1350 3100 2    60   Input ~ 0
GNDD
Text HLabel 1350 1750 2    60   Input ~ 0
INT2
Text Notes 600  700  0    60   ~ 0
PUERTOS DE EXPANSION 1 I/O
Text HLabel 3000 2050 2    60   Input ~ 0
SDA-i2c
Text HLabel 3000 2150 2    60   Input ~ 0
SDO-i2c
$Comp
L gateway-rescue:Conn_01x05_Male J?
U 1 1 5B4EB653
P 2600 1950
F 0 "J?" H 2600 2250 50  0000 C CNN
F 1 "Conn_01x05_Male" H 2600 1650 50  0000 C CNN
F 2 "" H 2600 1950 50  0001 C CNN
F 3 "" H 2600 1950 50  0001 C CNN
	1    2600 1950
	1    0    0    -1  
$EndComp
Text HLabel 3000 1850 2    60   Input ~ 0
VCCD
Text HLabel 3000 1950 2    60   Input ~ 0
GNDD
Text HLabel 3000 1750 2    60   Input ~ 0
INT3
Text Notes 2250 700  0    60   ~ 0
PUERTOS DE EXPANSION 2 I/O
Text Notes 600  5700 0    60   ~ 0
ICSP
Text Notes 600  4650 0    60   ~ 0
ETHERNET
Text HLabel 9700 2300 0    60   Input ~ 0
!REx
Text HLabel 9700 2400 0    60   Input ~ 0
DEx
Text HLabel 9700 2500 0    60   Input ~ 0
DI1
Text HLabel 9700 2600 0    60   Input ~ 0
RO1
Wire Wire Line
	1050 4850 1000 4850
Wire Wire Line
	1050 4950 1000 4950
Wire Wire Line
	1050 5050 1000 5050
Wire Wire Line
	1050 5150 1000 5150
Wire Wire Line
	1050 5250 1000 5250
Wire Wire Line
	1550 4850 1600 4850
Wire Wire Line
	1550 4950 1600 4950
Wire Wire Line
	1550 5050 1600 5050
Wire Wire Line
	1550 5150 1600 5150
Wire Wire Line
	1550 5250 1600 5250
Wire Wire Line
	10250 2100 9700 2100
Wire Wire Line
	10250 2200 9700 2200
Wire Wire Line
	8400 3950 7850 3950
Wire Wire Line
	8400 4050 7850 4050
Wire Wire Line
	10250 3850 9700 3850
Wire Wire Line
	10250 3950 9700 3950
Wire Wire Line
	9700 3250 10250 3250
Wire Wire Line
	9700 3350 10250 3350
Wire Wire Line
	9700 1450 10250 1450
Wire Wire Line
	9700 1550 10250 1550
Wire Wire Line
	10250 3450 9700 3450
Wire Wire Line
	10250 3550 9700 3550
Wire Wire Line
	10250 1650 9700 1650
Wire Wire Line
	9700 1750 10250 1750
Wire Wire Line
	8400 3400 7850 3400
Wire Wire Line
	8400 3500 7850 3500
Wire Wire Line
	8400 3600 7850 3600
Wire Wire Line
	8400 3700 7850 3700
Wire Notes Line
	10900 2850 9850 2850
Wire Notes Line
	9850 2850 9850 1400
Wire Notes Line
	9850 1400 10900 1400
Wire Notes Line
	10900 1400 10900 2850
Wire Notes Line
	9850 3150 10900 3150
Wire Notes Line
	10900 3150 10900 4500
Wire Notes Line
	10900 4500 9850 4500
Wire Notes Line
	9850 4500 9850 3150
Wire Notes Line
	8000 3300 9050 3300
Wire Notes Line
	9050 3300 9050 4450
Wire Notes Line
	9050 4450 8000 4450
Wire Notes Line
	8000 4450 8000 3300
Wire Wire Line
	1050 5850 1250 5850
Wire Wire Line
	1250 5950 1050 5950
Wire Wire Line
	1050 6050 1250 6050
Wire Wire Line
	1250 6150 1050 6150
Wire Wire Line
	1250 6250 1050 6250
Wire Wire Line
	1350 3950 1150 3950
Wire Wire Line
	1350 4050 1150 4050
Wire Wire Line
	1350 900  1150 900 
Wire Wire Line
	1350 1000 1150 1000
Wire Wire Line
	1350 1100 1150 1100
Wire Wire Line
	1350 1200 1150 1200
Wire Wire Line
	1350 1300 1150 1300
Wire Wire Line
	1350 1400 1150 1400
Wire Wire Line
	1350 1500 1150 1500
Wire Wire Line
	1350 1600 1150 1600
Wire Notes Line
	600  6500 1700 6500
Wire Notes Line
	1800 3800 700  3800
Wire Notes Line
	700  3500 1900 3500
Wire Notes Line
	1900 3500 1900 2800
Wire Notes Line
	1900 2800 700  2800
Wire Notes Line
	700  2800 700  3500
Wire Notes Line
	600  4650 600  5550
Wire Notes Line
	600  5550 2000 5550
Wire Notes Line
	2000 5550 2000 4650
Wire Notes Line
	2000 4650 600  4650
Wire Wire Line
	1350 1850 1150 1850
Wire Wire Line
	1350 1950 1150 1950
Wire Wire Line
	1150 2050 1350 2050
Wire Wire Line
	1350 2150 1150 2150
Wire Wire Line
	1150 1750 1350 1750
Wire Wire Line
	3000 900  2800 900 
Wire Wire Line
	3000 1000 2800 1000
Wire Wire Line
	3000 1100 2800 1100
Wire Wire Line
	3000 1200 2800 1200
Wire Wire Line
	3000 1300 2800 1300
Wire Wire Line
	3000 1400 2800 1400
Wire Wire Line
	3000 1500 2800 1500
Wire Wire Line
	3000 1600 2800 1600
Wire Notes Line
	650  2350 1850 2350
Wire Notes Line
	1850 2350 1850 700 
Wire Notes Line
	1850 700  650  700 
Wire Notes Line
	650  700  650  2350
Wire Notes Line
	600  2750 600  4450
Wire Notes Line
	600  4450 1950 4450
Wire Notes Line
	1950 4450 1950 2750
Wire Notes Line
	1950 2750 600  2750
Wire Notes Line
	1800 3800 1800 4350
Wire Notes Line
	1800 4350 700  4350
Wire Notes Line
	700  4350 700  3800
Wire Notes Line
	600  5700 1700 5700
Wire Notes Line
	1700 5700 1700 6500
Wire Notes Line
	600  5700 600  6500
Wire Wire Line
	1350 3000 1150 3000
Wire Wire Line
	1350 3100 1150 3100
Wire Wire Line
	1350 3200 1150 3200
Wire Wire Line
	1350 3300 1150 3300
Wire Wire Line
	3000 1850 2800 1850
Wire Wire Line
	3000 1950 2800 1950
Wire Wire Line
	2800 2050 3000 2050
Wire Wire Line
	3000 2150 2800 2150
Wire Wire Line
	2800 1750 3000 1750
Wire Notes Line
	2300 2350 3500 2350
Wire Notes Line
	3500 2350 3500 700 
Wire Notes Line
	3500 700  2300 700 
Wire Notes Line
	2300 700  2300 2350
Wire Wire Line
	1150 4150 1350 4150
Wire Wire Line
	1350 4250 1150 4250
Wire Wire Line
	10250 2300 9700 2300
Wire Wire Line
	10250 2400 9700 2400
Wire Wire Line
	10250 2500 9700 2500
Wire Wire Line
	10250 2600 9700 2600
Text HLabel 9700 4250 0    60   Input ~ 0
DI2
Text HLabel 9700 4350 0    60   Input ~ 0
RO2
Wire Wire Line
	10250 4050 9700 4050
Wire Wire Line
	10250 4150 9700 4150
Wire Wire Line
	10250 4250 9700 4250
Wire Wire Line
	10250 4350 9700 4350
Text HLabel 7850 4250 0    60   Input ~ 0
R1OUT
Text HLabel 7850 4150 0    60   Input ~ 0
T1IN
Wire Wire Line
	8400 4150 7850 4150
Wire Wire Line
	8400 4250 7850 4250
Text HLabel 7850 3700 0    60   Input ~ 0
RS232OUT
Text HLabel 9700 4050 0    60   Input ~ 0
!REx
Text HLabel 9700 4150 0    60   Input ~ 0
DEx
Text HLabel 9700 1750 0    60   Input ~ 0
GND1
Text Notes 3850 1700 0    60   ~ 0
SPI-PORT2 (MEM)
Wire Notes Line
	3850 2350 4950 2350
Wire Notes Line
	3850 1700 4950 1700
Wire Notes Line
	4950 1700 4950 2350
Wire Notes Line
	3850 1700 3850 2350
Text Notes 3850 700  0    60   ~ 0
SPI-PORT1 (RTCC)
Wire Notes Line
	3850 1350 4950 1350
Wire Notes Line
	3850 700  4950 700 
Wire Notes Line
	4950 700  4950 1350
Wire Notes Line
	3850 700  3850 1350
Text Notes 5200 700  0    60   ~ 0
SPI-PORT3 (RF)
Wire Notes Line
	5200 1350 6300 1350
Wire Notes Line
	5200 700  6300 700 
Wire Notes Line
	6300 700  6300 1350
Wire Notes Line
	5200 700  5200 1350
Text Notes 5200 1550 0    60   ~ 0
SPI-PORT4 (WIFI)
Wire Notes Line
	5200 2350 6300 2350
Wire Notes Line
	5200 1550 6300 1550
Wire Notes Line
	6300 1550 6300 2350
Wire Notes Line
	5200 1550 5200 2350
$Comp
L gateway-rescue:Conn_02x05_Odd_Even J?
U 1 1 5B4D691A
P 5700 1000
F 0 "J?" H 5750 1300 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 5750 700 50  0000 C CNN
F 2 "" H 5700 1000 50  0001 C CNN
F 3 "" H 5700 1000 50  0001 C CNN
	1    5700 1000
	1    0    0    -1  
$EndComp
Text HLabel 6050 800  2    60   Input ~ 0
SDI
Text HLabel 5450 1000 0    60   Input ~ 0
CS0
Text HLabel 5450 1100 0    60   Input ~ 0
VCCD
Text HLabel 5450 800  0    60   Input ~ 0
INT
Text HLabel 5450 900  0    60   Input ~ 0
SDO
Text HLabel 6050 900  2    60   Input ~ 0
SCK
Text HLabel 6050 1100 2    60   Input ~ 0
GNDD
Wire Wire Line
	5500 800  5450 800 
Wire Wire Line
	5500 900  5450 900 
Wire Wire Line
	5500 1000 5450 1000
Wire Wire Line
	5500 1100 5450 1100
Wire Wire Line
	6000 800  6050 800 
Wire Wire Line
	6000 900  6050 900 
Wire Wire Line
	6000 1000 6050 1000
Wire Wire Line
	6000 1100 6050 1100
Text HLabel 6050 1000 2    60   Input ~ 0
CE
$Comp
L gateway-rescue:Conn_02x05_Odd_Even J?
U 1 1 5B4D71F7
P 4350 1000
F 0 "J?" H 4400 1300 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4400 700 50  0000 C CNN
F 2 "" H 4350 1000 50  0001 C CNN
F 3 "" H 4350 1000 50  0001 C CNN
	1    4350 1000
	1    0    0    -1  
$EndComp
Text HLabel 4700 800  2    60   Input ~ 0
SDI
Text HLabel 4100 1200 0    60   Input ~ 0
CS1
Text HLabel 4100 1100 0    60   Input ~ 0
VCCD
Text HLabel 4100 800  0    60   Input ~ 0
INT
Text HLabel 4100 900  0    60   Input ~ 0
SDO
Text HLabel 4700 900  2    60   Input ~ 0
SCK
Text HLabel 4700 1100 2    60   Input ~ 0
GNDD
Wire Wire Line
	4150 800  4100 800 
Wire Wire Line
	4150 900  4100 900 
Wire Wire Line
	4150 1000 4100 1000
Wire Wire Line
	4150 1100 4100 1100
Wire Wire Line
	4650 800  4700 800 
Wire Wire Line
	4650 900  4700 900 
Wire Wire Line
	4650 1000 4700 1000
Wire Wire Line
	4650 1100 4700 1100
Text HLabel 4700 1000 2    60   Input ~ 0
CE
Text HLabel 5450 1850 0    60   Input ~ 0
SDI
Text HLabel 5450 2250 0    60   Input ~ 0
CS4
Text HLabel 5450 2050 0    60   Input ~ 0
VCCD
Text HLabel 5450 1750 0    60   Input ~ 0
INT
Text HLabel 6050 1750 2    60   Input ~ 0
SDO
Text HLabel 6050 1850 2    60   Input ~ 0
SCK
Text HLabel 6050 2050 2    60   Input ~ 0
GNDD
Wire Wire Line
	5500 1750 5450 1750
Wire Wire Line
	5500 1850 5450 1850
Wire Wire Line
	5500 1950 5450 1950
Wire Wire Line
	5500 2050 5450 2050
Wire Wire Line
	6000 1750 6050 1750
Wire Wire Line
	6000 1850 6050 1850
Wire Wire Line
	6000 1950 6050 1950
Wire Wire Line
	6000 2050 6050 2050
Text HLabel 6050 1950 2    60   Input ~ 0
CE
$Comp
L gateway-rescue:Conn_02x05_Odd_Even J?
U 1 1 5B4D72C7
P 4350 2000
F 0 "J?" H 4400 2300 50  0000 C CNN
F 1 "Conn_02x04_Odd_Even" H 4400 1700 50  0000 C CNN
F 2 "" H 4350 2000 50  0001 C CNN
F 3 "" H 4350 2000 50  0001 C CNN
	1    4350 2000
	1    0    0    -1  
$EndComp
Text HLabel 4700 1800 2    60   Input ~ 0
SDI
Text HLabel 4100 2100 0    60   Input ~ 0
VCCD
Text HLabel 4100 1800 0    60   Input ~ 0
INT
Text HLabel 4100 1900 0    60   Input ~ 0
SDO
Text HLabel 4700 1900 2    60   Input ~ 0
SCK
Text HLabel 4700 2100 2    60   Input ~ 0
GNDD
Wire Wire Line
	4150 1800 4100 1800
Wire Wire Line
	4150 1900 4100 1900
Wire Wire Line
	4150 2000 4100 2000
Wire Wire Line
	4150 2100 4100 2100
Wire Wire Line
	4650 1800 4700 1800
Wire Wire Line
	4650 1900 4700 1900
Wire Wire Line
	4650 2000 4700 2000
Wire Wire Line
	4650 2100 4700 2100
Text HLabel 4700 2000 2    60   Input ~ 0
CE
Text HLabel 4100 1000 0    60   Input ~ 0
CS0
Text HLabel 4100 2000 0    60   Input ~ 0
CS0
Text HLabel 5450 1200 0    60   Input ~ 0
CS1
Text HLabel 4100 2200 0    60   Input ~ 0
CS1
Text HLabel 6050 1200 2    60   Input ~ 0
CS3
Text HLabel 4700 2200 2    60   Input ~ 0
CS3
Text HLabel 4700 1200 2    60   Input ~ 0
CS3
Wire Wire Line
	4100 1200 4150 1200
Wire Wire Line
	4650 1200 4700 1200
Wire Wire Line
	4100 2200 4150 2200
Wire Wire Line
	4650 2200 4700 2200
Wire Wire Line
	5450 1200 5500 1200
Wire Wire Line
	6000 1200 6050 1200
$Comp
L gateway-rescue:Screw_Terminal_01x08 J?
U 1 1 5B4E5CEC
P 4400 2950
F 0 "J?" H 4400 3350 50  0000 C CNN
F 1 "Screw_Terminal_01x08" V 4550 2900 50  0000 C CNN
F 2 "" H 4400 2950 50  0001 C CNN
F 3 "" H 4400 2950 50  0001 C CNN
	1    4400 2950
	0    -1   -1   0   
$EndComp
$Comp
L gateway-rescue:Screw_Terminal_01x08 J?
U 1 1 5B4E5E2A
P 5350 7450
F 0 "J?" H 5350 7850 50  0000 C CNN
F 1 "Screw_Terminal_01x08" V 5500 7400 50  0000 C CNN
F 2 "" H 5350 7450 50  0001 C CNN
F 3 "" H 5350 7450 50  0001 C CNN
	1    5350 7450
	0    -1   1    0   
$EndComp
$Comp
L gateway-rescue:Conn_01x02_Male J?
U 1 1 5B4E7C0C
P 3550 3300
F 0 "J?" H 3550 3400 50  0000 C CNN
F 1 "Conn_01x02_Male" H 3450 3450 50  0000 C CNN
F 2 "" H 3550 3300 50  0001 C CNN
F 3 "" H 3550 3300 50  0001 C CNN
	1    3550 3300
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x05_Male J?
U 1 1 5B4E7D22
P 3750 3750
F 0 "J?" H 3750 3850 50  0000 C CNN
F 1 "Conn_01x05_Male" H 4050 4050 50  0000 C CNN
F 2 "" H 3750 3750 50  0001 C CNN
F 3 "" H 3750 3750 50  0001 C CNN
	1    3750 3750
	-1   0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x03_Male J?
U 1 1 5B4E81F0
P 4200 4350
F 0 "J?" H 4200 4450 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4300 4550 50  0000 C CNN
F 2 "" H 4200 4350 50  0001 C CNN
F 3 "" H 4200 4350 50  0001 C CNN
	1    4200 4350
	-1   0    0    -1  
$EndComp
$Comp
L gateway-rescue:Conn_01x02_Male J?
U 1 1 5B4E8348
P 4900 4050
F 0 "J?" H 4900 4150 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4900 3850 50  0000 C CNN
F 2 "" H 4900 4050 50  0001 C CNN
F 3 "" H 4900 4050 50  0001 C CNN
	1    4900 4050
	-1   0    0    1   
$EndComp
$Comp
L gateway-rescue:Conn_01x02_Male J?
U 1 1 5B4E8488
P 5550 3400
F 0 "J?" H 5550 3500 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5450 3200 50  0000 C CNN
F 2 "" H 5550 3400 50  0001 C CNN
F 3 "" H 5550 3400 50  0001 C CNN
	1    5550 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	3750 3300 4100 3300
Wire Wire Line
	4100 3300 4100 3150
Wire Wire Line
	3750 3400 4200 3400
Wire Wire Line
	4200 3400 4200 3150
Wire Wire Line
	4300 3150 4300 3950
Wire Wire Line
	4300 3950 4200 3950
$Comp
L gateway-rescue:Conn_01x02_Male J?
U 1 1 5B4E89D2
P 4000 3950
F 0 "J?" H 4000 4050 50  0000 C CNN
F 1 "Conn_01x03_Male" H 4100 4050 50  0000 C CNN
F 2 "" H 4000 3950 50  0001 C CNN
F 3 "" H 4000 3950 50  0001 C CNN
	1    4000 3950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3150 4400 4050
Wire Wire Line
	4400 4050 4200 4050
Wire Wire Line
	4800 3150 4800 3300
Wire Wire Line
	4800 3300 5350 3300
Wire Wire Line
	5350 3400 4700 3400
Wire Wire Line
	4700 3400 4700 3150
Wire Wire Line
	4700 3950 4600 3950
Wire Wire Line
	4600 3950 4600 3150
Wire Wire Line
	4700 4050 4500 4050
Wire Wire Line
	4500 4050 4500 3150
Text HLabel 3400 3750 0    60   Input ~ 0
GNDD
Text HLabel 5700 3750 2    60   Input ~ 0
GNDD
Text HLabel 3400 4450 0    60   Input ~ 0
GNDD
Text HLabel 5700 4450 2    60   Input ~ 0
GNDD
Wire Wire Line
	3400 3650 3550 3650
Text HLabel 3400 3550 0    60   Input ~ 0
AN2
$Comp
L gateway-rescue:Screw_Terminal_01x03 J?
U 1 1 5B4EB129
P 5050 2950
F 0 "J?" H 5050 3150 50  0000 C CNN
F 1 "Screw_Terminal_01x03" V 5200 2650 50  0000 C CNN
F 2 "" H 5050 2950 50  0001 C CNN
F 3 "" H 5050 2950 50  0001 C CNN
	1    5050 2950
	0    -1   -1   0   
$EndComp
Text HLabel 5700 4250 2    60   Input ~ 0
AN4
Text HLabel 3400 4250 0    60   Input ~ 0
AN3
Text HLabel 5700 3550 2    60   Input ~ 0
AN10
Wire Wire Line
	3400 3550 3550 3550
Text HLabel 3400 3650 0    60   Input ~ 0
VCCD
Text HLabel 3400 3850 0    60   Input ~ 0
VCCA
Text HLabel 3400 4350 0    60   Input ~ 0
VCCD
Text HLabel 3400 3950 0    60   Input ~ 0
GNDA
Wire Wire Line
	3400 3750 3550 3750
Wire Wire Line
	3400 3850 3550 3850
Wire Wire Line
	3400 3950 3550 3950
Wire Wire Line
	3400 4250 4000 4250
Wire Wire Line
	4000 4350 3400 4350
Wire Wire Line
	4000 4450 3400 4450
$Comp
L gateway-rescue:Conn_01x03_Male J?
U 1 1 5B4EEDDC
P 4700 4350
F 0 "J?" H 4700 4450 50  0000 C CNN
F 1 "Conn_01x02_Male" H 4900 4550 50  0000 C CNN
F 2 "" H 4700 4350 50  0001 C CNN
F 3 "" H 4700 4350 50  0001 C CNN
	1    4700 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 4250 4900 4250
Wire Wire Line
	4900 4350 5700 4350
Wire Wire Line
	4900 4450 5700 4450
Text HLabel 5700 4350 2    60   Input ~ 0
VCCD
Text HLabel 5700 6200 0    60   Input ~ 0
GNDD
$Comp
L gateway-rescue:Conn_01x03_Male J?
U 1 1 5B4EF665
P 5350 3650
F 0 "J?" H 5350 3750 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5650 3850 50  0000 C CNN
F 2 "" H 5350 3650 50  0001 C CNN
F 3 "" H 5350 3650 50  0001 C CNN
	1    5350 3650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5700 3550 5550 3550
Wire Wire Line
	5550 3650 5700 3650
Wire Wire Line
	5550 3750 5700 3750
Text HLabel 5700 3650 2    60   Input ~ 0
VCCD
Wire Notes Line
	4550 4550 4950 4550
Wire Notes Line
	4950 4550 4950 3800
Wire Notes Line
	4950 3800 4550 3800
Wire Notes Line
	4550 3800 4550 4550
Wire Notes Line
	3950 4550 4350 4550
Wire Notes Line
	3950 4550 3950 3800
Wire Notes Line
	3950 3800 4350 3800
Wire Notes Line
	4350 3800 4350 4550
Wire Notes Line
	5250 3150 5250 3900
Wire Notes Line
	5250 3900 5650 3900
Wire Notes Line
	5650 3900 5650 3150
Wire Notes Line
	5650 3150 5250 3150
Wire Notes Line
	3850 4050 3450 4050
Wire Notes Line
	3450 4050 3450 3150
Wire Notes Line
	3450 3150 3850 3150
Wire Notes Line
	3850 3150 3850 4050
Text Notes 3500 4800 0    60   ~ 0
SLOT\nIN-0
Text Notes 4050 4800 0    60   ~ 0
SLOT\nIN-1
Text Notes 4650 4800 0    60   ~ 0
SLOT\nIN-2
Text Notes 5350 4800 0    60   ~ 0
SLOT\nIN-3
Text Notes 3700 2750 0    60   ~ 0
BAHIAS PARA MODULOS ADAPTADORES\nDE ENTRADAS Y ALIMENTACIONES SENSORES
Text HLabel 4950 3500 0    60   Input ~ 0
GND
Text HLabel 5000 3600 0    60   Input ~ 0
12V
Text HLabel 5100 3700 0    60   Input ~ 0
5V
Wire Wire Line
	4950 3150 4950 3150
Wire Wire Line
	4950 3150 4950 3500
Wire Wire Line
	5000 3600 5050 3600
Wire Wire Line
	5050 3600 5050 3150
Wire Wire Line
	5100 3700 5150 3700
Wire Wire Line
	5150 3700 5150 3150
Text Notes 3950 5200 0    60   ~ 0
SALIDAS POR RELE Y TRANSISTOR
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	3750 2400 6500 2400
Wire Notes Line
	6500 2400 6500 600 
Wire Notes Line
	6500 600  3750 600 
Wire Notes Line
	3750 600  3750 2400
Text Notes 4100 1600 0    51   ~ 0
BAHIAS DE EXPANSION SPI\nSE MONTAN VERTICALMENTE
Wire Notes Line
	2900 5000 6500 5000
Wire Notes Line
	6500 5000 6500 2550
Wire Notes Line
	6500 2550 2900 2550
Wire Notes Line
	2900 2550 2900 5000
Wire Notes Line
	11100 4700 7250 4700
Wire Notes Line
	7250 4700 7250 650 
Wire Notes Line
	7250 650  11100 650 
Wire Notes Line
	11100 650  11100 4700
Text Notes 8350 1000 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
$Comp
L gateway-rescue:4N25 U?
U 1 1 5B4F57D5
P 6000 5950
F 0 "U?" H 5800 6150 50  0000 L CNN
F 1 "4N25" H 6000 6150 50  0000 L CNN
F 2 "Housings_DIP:DIP-6_W7.62mm" H 5800 5750 50  0001 L CIN
F 3 "" H 6000 5950 50  0001 L CNN
	1    6000 5950
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:FINDER-32.21-x300 K?
U 1 1 5B4F593B
P 3000 6650
F 0 "K?" H 3450 6800 50  0000 L CNN
F 1 "FINDER-32.21-x300" H 2350 6150 50  0000 L CNN
F 2 "Relays_THT:Relay_SPST_Finder_32.21-x300" H 4270 6620 50  0001 C CNN
F 3 "" H 3000 6650 50  0001 C CNN
	1    3000 6650
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:FINDER-32.21-x300 K?
U 1 1 5B4F5A0F
P 3900 6650
F 0 "K?" H 4350 6800 50  0000 L CNN
F 1 "FINDER-32.21-x300" H 3150 6050 50  0000 L CNN
F 2 "Relays_THT:Relay_SPST_Finder_32.21-x300" H 5170 6620 50  0001 C CNN
F 3 "" H 3900 6650 50  0001 C CNN
	1    3900 6650
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:FINDER-32.21-x300 K?
U 1 1 5B4F5D47
P 4800 6650
F 0 "K?" H 5250 6800 50  0000 L CNN
F 1 "FINDER-32.21-x300" H 4000 5950 50  0000 L CNN
F 2 "Relays_THT:Relay_SPST_Finder_32.21-x300" H 6070 6620 50  0001 C CNN
F 3 "" H 4800 6650 50  0001 C CNN
	1    4800 6650
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:TIP122 Q?
U 1 1 5B4F6689
P 5900 6650
F 0 "Q?" H 6150 6725 50  0000 L CNN
F 1 "TIP122" H 6150 6650 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-220_Vertical" H 6150 6575 50  0001 L CIN
F 3 "" H 5900 6650 50  0001 L CNN
	1    5900 6650
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 6850 6000 6900
Wire Wire Line
	6000 6900 5650 6900
Wire Wire Line
	5650 6900 5650 7250
Wire Wire Line
	6000 6450 6400 6450
Wire Wire Line
	5750 7050 6400 7050
Wire Wire Line
	5750 7050 5750 7250
Wire Wire Line
	6300 6050 6300 6400
Wire Wire Line
	6300 6400 5700 6400
Wire Wire Line
	5700 6400 5700 6650
Text HLabel 6500 5450 2    60   Input ~ 0
VCC
$Comp
L gateway-rescue:R R?
U 1 1 5B4F7741
P 6350 5650
F 0 "R?" V 6450 5650 50  0000 C CNN
F 1 "R" V 6350 5650 50  0000 C CNN
F 2 "" V 6280 5650 50  0001 C CNN
F 3 "" H 6350 5650 50  0001 C CNN
	1    6350 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6350 5500 6500 5500
Wire Wire Line
	6500 5500 6500 5450
Wire Wire Line
	6350 5800 6350 5850
Wire Wire Line
	6350 5850 6300 5850
Wire Wire Line
	5700 6200 5700 6050
$Comp
L gateway-rescue:R R?
U 1 1 5B4F7D3D
P 5650 5650
F 0 "R?" V 5750 5650 50  0000 C CNN
F 1 "R" V 5650 5650 50  0000 C CNN
F 2 "" V 5580 5650 50  0001 C CNN
F 3 "" H 5650 5650 50  0001 C CNN
	1    5650 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	5650 5800 5650 5850
Wire Wire Line
	5650 5850 5700 5850
Text HLabel 5650 5450 0    60   Input ~ 0
AN9
Wire Wire Line
	5650 5450 5650 5500
$Comp
L gateway-rescue:R R?
U 1 1 5B4F835C
P 6500 5650
F 0 "R?" V 6600 5650 50  0000 C CNN
F 1 "R" V 6500 5650 50  0000 C CNN
F 2 "" V 6430 5650 50  0001 C CNN
F 3 "" H 6500 5650 50  0001 C CNN
	1    6500 5650
	-1   0    0    1   
$EndComp
Wire Wire Line
	6300 5950 6500 5950
Wire Wire Line
	6500 5950 6500 5800
Wire Wire Line
	6500 5500 6500 5500
Connection ~ 6500 5500
$Comp
L gateway-rescue:Polyfuse F?
U 1 1 5B4F8867
P 6400 6850
F 0 "F?" V 6300 6850 50  0000 C CNN
F 1 "Polyfuse" V 6500 6850 50  0000 C CNN
F 2 "" H 6450 6650 50  0001 L CNN
F 3 "" H 6400 6850 50  0001 C CNN
	1    6400 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 6450 6400 6700
Wire Wire Line
	6400 7050 6400 7000
Wire Wire Line
	5450 7250 5450 7000
Wire Wire Line
	5450 7000 5000 7000
Wire Wire Line
	5000 7000 5000 6950
Wire Wire Line
	5550 6350 5550 7250
Wire Wire Line
	5100 6350 5550 6350
Wire Wire Line
	5250 7250 5250 7100
Wire Wire Line
	5250 7100 4100 7100
Wire Wire Line
	4100 7100 4100 6950
Wire Wire Line
	5350 7250 5350 7050
Wire Wire Line
	5350 7050 4350 7050
Wire Wire Line
	4350 7050 4350 6350
Wire Wire Line
	4350 6350 4200 6350
Wire Wire Line
	5150 7250 5150 7150
Wire Wire Line
	5150 7150 3450 7150
Wire Wire Line
	3450 7150 3450 6350
Wire Wire Line
	3450 6350 3300 6350
Wire Wire Line
	3200 6950 3200 7200
Wire Wire Line
	3200 7200 5050 7200
Wire Wire Line
	5050 7200 5050 7250
Text HLabel 2700 5750 0    60   Input ~ 0
AN0
Text HLabel 3600 5750 0    60   Input ~ 0
AN1
Text HLabel 4550 5750 0    60   Input ~ 0
AN5
$Comp
L gateway-rescue:1N4148 D?
U 1 1 5B4FB99B
P 2650 6200
F 0 "D?" H 2650 6300 50  0000 C CNN
F 1 "1N4148" H 2650 6100 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_Horizontal_RM10" H 2650 6025 50  0001 C CNN
F 3 "" H 2650 6200 50  0001 C CNN
	1    2650 6200
	0    1    1    0   
$EndComp
$Comp
L gateway-rescue:1N4148 D?
U 1 1 5B4FBA9D
P 3550 6200
F 0 "D?" H 3550 6300 50  0000 C CNN
F 1 "1N4148" H 3550 6100 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_Horizontal_RM10" H 3550 6025 50  0001 C CNN
F 3 "" H 3550 6200 50  0001 C CNN
	1    3550 6200
	0    1    1    0   
$EndComp
$Comp
L gateway-rescue:1N4148 D?
U 1 1 5B4FBC2F
P 4450 6200
F 0 "D?" H 4450 6300 50  0000 C CNN
F 1 "1N4148" H 4450 6100 50  0000 C CNN
F 2 "Diodes_THT:D_DO-35_SOD27_Horizontal_RM10" H 4450 6025 50  0001 C CNN
F 3 "" H 4450 6200 50  0001 C CNN
	1    4450 6200
	0    1    1    0   
$EndComp
$Comp
L gateway-rescue:R R?
U 1 1 5B4FBDDD
P 2800 6200
F 0 "R?" V 2900 6200 50  0000 C CNN
F 1 "R" V 2800 6200 50  0000 C CNN
F 2 "" V 2730 6200 50  0001 C CNN
F 3 "" H 2800 6200 50  0001 C CNN
	1    2800 6200
	-1   0    0    1   
$EndComp
$Comp
L gateway-rescue:R R?
U 1 1 5B4FC01D
P 3700 6200
F 0 "R?" V 3800 6200 50  0000 C CNN
F 1 "R" V 3700 6200 50  0000 C CNN
F 2 "" V 3630 6200 50  0001 C CNN
F 3 "" H 3700 6200 50  0001 C CNN
	1    3700 6200
	-1   0    0    1   
$EndComp
$Comp
L gateway-rescue:R R?
U 1 1 5B4FC117
P 4600 6200
F 0 "R?" V 4700 6200 50  0000 C CNN
F 1 "R" V 4600 6200 50  0000 C CNN
F 2 "" V 4530 6200 50  0001 C CNN
F 3 "" H 4600 6200 50  0001 C CNN
	1    4600 6200
	-1   0    0    1   
$EndComp
Text HLabel 2650 7000 0    60   Input ~ 0
GND
Wire Wire Line
	2650 7000 2800 7000
Wire Wire Line
	2800 7000 2800 6950
Text HLabel 3650 7000 0    60   Input ~ 0
GND
Wire Wire Line
	3650 7000 3700 7000
Wire Wire Line
	3700 7000 3700 6950
Text HLabel 4550 7000 0    60   Input ~ 0
GND
Wire Wire Line
	4550 7000 4600 7000
Wire Wire Line
	4600 7000 4600 6950
Wire Wire Line
	2650 6050 2800 6050
Wire Wire Line
	3550 6050 3700 6050
Wire Wire Line
	4450 6050 4600 6050
Wire Wire Line
	2650 6350 2650 6950
Wire Wire Line
	2650 6950 2800 6950
Wire Wire Line
	3550 6350 3550 6950
Wire Wire Line
	3550 6950 3700 6950
Wire Wire Line
	4450 6350 4450 6950
Wire Wire Line
	4450 6950 4600 6950
Wire Wire Line
	2800 6350 3150 6350
Wire Wire Line
	3700 6350 4100 6350
Wire Wire Line
	4600 6350 5000 6350
$Comp
L gateway-rescue:R R?
U 1 1 5B4FE6D3
P 2850 5900
F 0 "R?" V 2950 5900 50  0000 C CNN
F 1 "R" V 2850 5900 50  0000 C CNN
F 2 "" V 2780 5900 50  0001 C CNN
F 3 "" H 2850 5900 50  0001 C CNN
	1    2850 5900
	-1   0    0    1   
$EndComp
$Comp
L gateway-rescue:R R?
U 1 1 5B4FE89B
P 3800 5900
F 0 "R?" V 3900 5900 50  0000 C CNN
F 1 "R" V 3800 5900 50  0000 C CNN
F 2 "" V 3730 5900 50  0001 C CNN
F 3 "" H 3800 5900 50  0001 C CNN
	1    3800 5900
	-1   0    0    1   
$EndComp
$Comp
L gateway-rescue:R R?
U 1 1 5B4FE9A1
P 4700 5900
F 0 "R?" V 4800 5900 50  0000 C CNN
F 1 "R" V 4700 5900 50  0000 C CNN
F 2 "" V 4630 5900 50  0001 C CNN
F 3 "" H 4700 5900 50  0001 C CNN
	1    4700 5900
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 5750 3800 5750
Wire Wire Line
	4550 5750 4700 5750
Text HLabel 3100 5700 0    60   Input ~ 0
VCC
Text HLabel 4050 5700 0    60   Input ~ 0
VCC
Text HLabel 4950 5700 0    60   Input ~ 0
VCC
$Comp
L gateway-rescue:BC337 Q?
U 1 1 5B4FF9C7
P 3050 6050
F 0 "Q?" H 3250 6125 50  0000 L CNN
F 1 "BC337" H 3250 6050 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Narrow" H 3250 5975 50  0001 L CIN
F 3 "" H 3050 6050 50  0001 L CNN
	1    3050 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	2700 5750 2850 5750
Wire Wire Line
	3100 5700 3150 5700
Wire Wire Line
	3150 5700 3150 5850
Wire Wire Line
	3150 6350 3150 6250
Connection ~ 2800 6350
$Comp
L gateway-rescue:BC337 Q?
U 1 1 5B500624
P 4000 6050
F 0 "Q?" H 4200 6125 50  0000 L CNN
F 1 "BC337" H 4200 6050 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Narrow" H 4200 5975 50  0001 L CIN
F 3 "" H 4000 6050 50  0001 L CNN
	1    4000 6050
	1    0    0    -1  
$EndComp
$Comp
L gateway-rescue:BC337 Q?
U 1 1 5B500728
P 4900 6050
F 0 "Q?" H 5100 6125 50  0000 L CNN
F 1 "BC337" H 5100 6050 50  0000 L CNN
F 2 "TO_SOT_Packages_THT:TO-92_Molded_Narrow" H 5100 5975 50  0001 L CIN
F 3 "" H 4900 6050 50  0001 L CNN
	1    4900 6050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4050 5700 4100 5700
Wire Wire Line
	4100 5700 4100 5850
Wire Wire Line
	4100 6350 4100 6250
Connection ~ 3700 6350
Wire Wire Line
	5000 6350 5000 6250
Wire Wire Line
	4950 5700 5000 5700
Wire Wire Line
	5000 5700 5000 5850
Wire Notes Line
	6850 7600 2250 7600
Wire Notes Line
	2250 7600 2250 5050
Wire Notes Line
	2250 5050 6900 5050
Wire Notes Line
	6900 5050 6900 7600
Wire Notes Line
	6900 7600 6800 7600
Text HLabel 5450 1950 0    60   Input ~ 0
CS0
Text HLabel 5450 2150 0    60   Input ~ 0
CS1
Text HLabel 6050 2150 2    60   Input ~ 0
CS3
$EndSCHEMATC
