EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 2
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L board-rs484-rescue:MAX485E U5
U 1 1 5B4FE404
P 6700 3400
F 0 "U5" H 6460 3850 50  0000 C CNN
F 1 "MAX485E" H 6730 3850 50  0000 L CNN
F 2 "Housings_DIP:DIP-8_W7.62mm_Socket_LongPads" H 6700 2700 50  0001 C CNN
F 3 "" H 6700 3450 50  0001 C CNN
	1    6700 3400
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:Polyfuse_Small F1
U 1 1 5B4FE405
P 7950 3200
F 0 "F1" V 7875 3200 50  0000 C CNN
F 1 "Polyfuse_Small" V 8050 3200 50  0000 C CNN
F 2 "Varistors:RV_Disc_D7_W4.5_P5" H 8000 3000 50  0001 L CNN
F 3 "" H 7950 3200 50  0001 C CNN
	1    7950 3200
	0    -1   -1   0   
$EndComp
$Comp
L board-rs484-rescue:Polyfuse_Small F2
U 1 1 5B4FE406
P 7950 3800
F 0 "F2" V 7875 3800 50  0000 C CNN
F 1 "Polyfuse_Small" V 8050 3800 50  0000 C CNN
F 2 "Varistors:RV_Disc_D7_W4.5_P5" H 8000 3600 50  0001 L CNN
F 3 "" H 7950 3800 50  0001 C CNN
	1    7950 3800
	0    -1   -1   0   
$EndComp
$Comp
L board-rs484-rescue:D_TVS D2
U 1 1 5B4FE407
P 7750 3450
F 0 "D2" H 7750 3550 50  0000 C CNN
F 1 "D_TVS" H 7750 3350 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P10.16mm_Horizontal" H 7750 3450 50  0001 C CNN
F 3 "" H 7750 3450 50  0001 C CNN
	1    7750 3450
	0    1    1    0   
$EndComp
$Comp
L board-rs484-rescue:D_TVS D1
U 1 1 5B4FE408
P 7750 3000
F 0 "D1" H 7750 3100 50  0000 C CNN
F 1 "D_TVS" H 7750 2900 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P10.16mm_Horizontal" H 7750 3000 50  0001 C CNN
F 3 "" H 7750 3000 50  0001 C CNN
	1    7750 3000
	0    1    1    0   
$EndComp
$Comp
L board-rs484-rescue:D_TVS D3
U 1 1 5B4FE409
P 7750 4000
F 0 "D3" H 7750 4100 50  0000 C CNN
F 1 "D_TVS" H 7750 3900 50  0000 C CNN
F 2 "Diodes_THT:D_A-405_P10.16mm_Horizontal" H 7750 4000 50  0001 C CNN
F 3 "" H 7750 4000 50  0001 C CNN
	1    7750 4000
	0    1    1    0   
$EndComp
$Comp
L board-rs484-rescue:Varistor RV1
U 1 1 5B4FE40C
P 8550 3350
F 0 "RV1" V 8675 3350 50  0000 C CNN
F 1 "Varistor" V 8425 3350 50  0000 C CNN
F 2 "Varistors:RV_Disc_D7_W4_P5" V 8480 3350 50  0001 C CNN
F 3 "" H 8550 3350 50  0001 C CNN
	1    8550 3350
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:R R9
U 1 1 5B4FE40F
P 7250 3300
F 0 "R9" V 7330 3300 50  0000 C CNN
F 1 "R" V 7250 3300 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P25.40mm_Horizontal" V 7180 3300 50  0001 C CNN
F 3 "" H 7250 3300 50  0001 C CNN
	1    7250 3300
	0    1    1    0   
$EndComp
$Comp
L board-rs484-rescue:R R10
U 1 1 5B4FE410
P 7250 3600
F 0 "R10" V 7330 3600 50  0000 C CNN
F 1 "R" V 7250 3600 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0414_L11.9mm_D4.5mm_P25.40mm_Horizontal" V 7180 3600 50  0001 C CNN
F 3 "" H 7250 3600 50  0001 C CNN
	1    7250 3600
	0    1    1    0   
$EndComp
$Comp
L board-rs484-rescue:R R11
U 1 1 5B4FE412
P 7500 3050
F 0 "R11" V 7580 3050 50  0000 C CNN
F 1 "R" V 7500 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7430 3050 50  0001 C CNN
F 3 "" H 7500 3050 50  0001 C CNN
	1    7500 3050
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R12
U 1 1 5B4FE413
P 7500 3450
F 0 "R12" V 7580 3450 50  0000 C CNN
F 1 "R" V 7500 3450 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7430 3450 50  0001 C CNN
F 3 "" H 7500 3450 50  0001 C CNN
	1    7500 3450
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R13
U 1 1 5B4FE414
P 7500 4000
F 0 "R13" V 7580 4000 50  0000 C CNN
F 1 "R" V 7500 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 7430 4000 50  0001 C CNN
F 3 "" H 7500 4000 50  0001 C CNN
	1    7500 4000
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:PC817 U1
U 1 1 5B4FE41B
P 4900 2800
F 0 "U1" H 4700 3000 50  0000 L CNN
F 1 "PC817" H 4900 3000 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm_Socket_LongPads" H 4700 2600 50  0001 L CIN
F 3 "" H 4900 2800 50  0001 L CNN
	1    4900 2800
	-1   0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:PC817 U2
U 1 1 5B4FE41C
P 4900 3700
F 0 "U2" H 4700 3900 50  0000 L CNN
F 1 "PC817" H 4900 3900 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm_Socket_LongPads" H 4700 3500 50  0001 L CIN
F 3 "" H 4900 3700 50  0001 L CNN
	1    4900 3700
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:PC817 U3
U 1 1 5B4FE41D
P 4900 4750
F 0 "U3" H 4700 4950 50  0000 L CNN
F 1 "PC817" H 4900 4950 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm_Socket_LongPads" H 4700 4550 50  0001 L CIN
F 3 "" H 4900 4750 50  0001 L CNN
	1    4900 4750
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:PC817 U4
U 1 1 5B4FE41E
P 4900 5600
F 0 "U4" H 4700 5800 50  0000 L CNN
F 1 "PC817" H 4900 5800 50  0000 L CNN
F 2 "Housings_DIP:DIP-4_W7.62mm_Socket_LongPads" H 4700 5400 50  0001 L CIN
F 3 "" H 4900 5600 50  0001 L CNN
	1    4900 5600
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:R R5
U 1 1 5B4FE41F
P 5300 3050
F 0 "R5" H 5380 3050 50  0000 C CNN
F 1 "R" V 5300 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5230 3050 50  0001 C CNN
F 3 "" H 5300 3050 50  0001 C CNN
	1    5300 3050
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R6
U 1 1 5B4FE420
P 5300 4000
F 0 "R6" V 5380 4000 50  0000 C CNN
F 1 "R" V 5300 4000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5230 4000 50  0001 C CNN
F 3 "" H 5300 4000 50  0001 C CNN
	1    5300 4000
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R7
U 1 1 5B4FE421
P 5300 5000
F 0 "R7" V 5380 5000 50  0000 C CNN
F 1 "R" V 5300 5000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5230 5000 50  0001 C CNN
F 3 "" H 5300 5000 50  0001 C CNN
	1    5300 5000
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R8
U 1 1 5B4FE422
P 5300 5850
F 0 "R8" V 5400 5850 50  0000 C CNN
F 1 "R" V 5300 5850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 5230 5850 50  0001 C CNN
F 3 "" H 5300 5850 50  0001 C CNN
	1    5300 5850
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R4
U 1 1 5B4FE423
P 4400 5850
F 0 "R4" V 4480 5850 50  0000 C CNN
F 1 "R" V 4400 5850 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4330 5850 50  0001 C CNN
F 3 "" H 4400 5850 50  0001 C CNN
	1    4400 5850
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R1
U 1 1 5B4FE424
P 4250 3050
F 0 "R1" V 4330 3050 50  0000 C CNN
F 1 "R" V 4250 3050 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4180 3050 50  0001 C CNN
F 3 "" H 4250 3050 50  0001 C CNN
	1    4250 3050
	-1   0    0    1   
$EndComp
Text HLabel 2600 3300 0    60   Input ~ 0
VD
Text HLabel 2600 3400 0    60   Input ~ 0
GndD
Text HLabel 2600 3600 0    60   Input ~ 0
!re
Text HLabel 2600 3700 0    60   Input ~ 0
de
Text HLabel 2600 3800 0    60   Input ~ 0
di
Text HLabel 2600 3500 0    60   Input ~ 0
ro
Text HLabel 5250 3600 2    60   Input ~ 0
Va
Text HLabel 5250 4650 2    60   Input ~ 0
Va
Text HLabel 5250 5500 2    60   Input ~ 0
Va
Text HLabel 5400 3200 2    60   Input ~ 0
Ga
Text HLabel 5400 5150 2    60   Input ~ 0
Ga
Text HLabel 5400 4150 2    60   Input ~ 0
Ga
Text HLabel 5400 6000 2    60   Input ~ 0
Ga
Text HLabel 9750 3550 2    60   Input ~ 0
Ga
Text HLabel 9750 3250 2    60   Input ~ 0
Va
Text HLabel 9750 3350 2    60   Input ~ 0
B
Text HLabel 9750 3450 2    60   Input ~ 0
A
$Comp
L power1:Earth_Protective #PWR016
U 1 1 5B5031A3
P 8850 3500
F 0 "#PWR016" H 9100 3250 50  0001 C CNN
F 1 "Earth_Protective" H 9300 3350 50  0001 C CNN
F 2 "" H 8850 3400 50  0001 C CNN
F 3 "" H 8850 3400 50  0001 C CNN
	1    8850 3500
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:Varistor RV2
U 1 1 5B503467
P 8550 3650
F 0 "RV2" V 8675 3650 50  0000 C CNN
F 1 "Varistor" V 8425 3650 50  0000 C CNN
F 2 "Varistors:RV_Disc_D7_W4_P5" V 8480 3650 50  0001 C CNN
F 3 "" H 8550 3650 50  0001 C CNN
	1    8550 3650
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:R R3
U 1 1 5B4FE426
P 4400 5000
F 0 "R3" H 4500 5000 50  0000 C CNN
F 1 "R" V 4400 5000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4330 5000 50  0001 C CNN
F 3 "" H 4400 5000 50  0001 C CNN
	1    4400 5000
	-1   0    0    1   
$EndComp
$Comp
L board-rs484-rescue:R R2
U 1 1 5B4FE425
P 4400 3950
F 0 "R2" V 4480 3950 50  0000 C CNN
F 1 "R" V 4400 3950 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 4330 3950 50  0001 C CNN
F 3 "" H 4400 3950 50  0001 C CNN
	1    4400 3950
	-1   0    0    1   
$EndComp
Wire Wire Line
	8050 3200 9250 3200
Connection ~ 8550 3200
Wire Wire Line
	7500 3200 7850 3200
Wire Wire Line
	7750 3150 7750 3300
Connection ~ 7750 3200
Wire Wire Line
	8050 3800 9250 3800
Connection ~ 8550 3800
Wire Wire Line
	7500 3300 7500 3200
Wire Wire Line
	7500 3600 7500 3850
Wire Wire Line
	7400 3600 7500 3600
Wire Wire Line
	7400 3300 7500 3300
Wire Wire Line
	7750 3600 7750 3850
Wire Wire Line
	7500 3800 7850 3800
Connection ~ 7750 3800
Connection ~ 7500 3800
Connection ~ 7750 4150
Connection ~ 7500 4150
Wire Wire Line
	6700 2850 9300 2850
Wire Wire Line
	9250 3200 9250 3350
Wire Wire Line
	9250 3350 9750 3350
Wire Wire Line
	9250 3800 9250 3450
Wire Wire Line
	9250 3450 9750 3450
Wire Wire Line
	9300 4150 9300 3550
Wire Wire Line
	9300 3550 9750 3550
Wire Wire Line
	9300 2850 9300 3250
Wire Wire Line
	9300 3250 9750 3250
Connection ~ 7750 2850
Wire Wire Line
	7100 3300 7100 3300
Wire Wire Line
	7100 3600 7100 3600
Wire Wire Line
	6700 4150 9300 4150
Wire Wire Line
	2600 3300 3900 3300
Wire Wire Line
	5700 3400 6300 3400
Wire Wire Line
	5800 3500 6300 3500
Wire Wire Line
	5900 3600 6300 3600
Wire Wire Line
	5900 3300 6300 3300
Wire Wire Line
	7500 2900 7500 2850
Connection ~ 7500 2850
Wire Wire Line
	2600 3700 4150 3700
Wire Wire Line
	2600 3800 3900 3800
Wire Wire Line
	2600 3600 4600 3600
Wire Wire Line
	3900 3300 3900 2700
Wire Wire Line
	3900 2700 4600 2700
Wire Wire Line
	4600 5700 4400 5700
Wire Wire Line
	4600 4850 4400 4850
Wire Wire Line
	4600 3800 4400 3800
Wire Wire Line
	4250 3200 4250 6000
Wire Wire Line
	4250 6000 4400 6000
Wire Wire Line
	4150 3700 4150 4650
Wire Wire Line
	2600 3500 4150 3500
Wire Wire Line
	4150 4650 4600 4650
Wire Wire Line
	3900 3800 3900 5500
Wire Wire Line
	3900 5500 4600 5500
Wire Wire Line
	5200 2900 5300 2900
Wire Wire Line
	5800 4850 5800 3500
Wire Wire Line
	5250 4650 5200 4650
Wire Wire Line
	5250 3600 5200 3600
Wire Wire Line
	5200 5500 5250 5500
Wire Wire Line
	5400 6000 5300 6000
Wire Wire Line
	5200 4850 5800 4850
Wire Wire Line
	5200 3800 5700 3800
Wire Wire Line
	5400 3200 5300 3200
Wire Wire Line
	5900 3300 5900 2700
Wire Wire Line
	5900 2700 5200 2700
Wire Wire Line
	5700 3800 5700 3400
Wire Wire Line
	5900 5700 5900 3600
Wire Wire Line
	2600 3400 4250 3400
Connection ~ 4250 3400
Connection ~ 9450 3350
Connection ~ 9450 3450
Connection ~ 3000 3300
Connection ~ 3000 3400
Connection ~ 3000 3500
Connection ~ 3000 3600
Connection ~ 3000 3700
Connection ~ 3000 3800
Wire Wire Line
	6700 2850 6700 2900
Connection ~ 9450 3550
Connection ~ 9450 3250
Wire Wire Line
	6700 4150 6700 4000
Connection ~ 8550 3500
Wire Wire Line
	8850 3500 8550 3500
Connection ~ 4250 4100
Connection ~ 4250 5150
Wire Wire Line
	4400 5150 4250 5150
Wire Wire Line
	4400 4100 4250 4100
$Comp
L board-rs484-rescue:LED D4
U 1 1 5B591659
P 3150 2600
F 0 "D4" H 3150 2700 50  0000 C CNN
F 1 "LED" H 3150 2500 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 3150 2600 50  0001 C CNN
F 3 "" H 3150 2600 50  0001 C CNN
	1    3150 2600
	0    -1   -1   0   
$EndComp
$Comp
L board-rs484-rescue:LED D5
U 1 1 5B5917C3
P 3450 2600
F 0 "D5" H 3450 2700 50  0000 C CNN
F 1 "LED" H 3450 2500 50  0000 C CNN
F 2 "LEDs:LED_D5.0mm" H 3450 2600 50  0001 C CNN
F 3 "" H 3450 2600 50  0001 C CNN
	1    3450 2600
	0    -1   -1   0   
$EndComp
$Comp
L board-rs484-rescue:R R14
U 1 1 5B591862
P 3150 3000
F 0 "R14" H 3230 3000 50  0000 C CNN
F 1 "R" V 3150 3000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3080 3000 50  0001 C CNN
F 3 "" H 3150 3000 50  0001 C CNN
	1    3150 3000
	1    0    0    -1  
$EndComp
$Comp
L board-rs484-rescue:R R15
U 1 1 5B591ABB
P 3450 3000
F 0 "R15" H 3530 3000 50  0000 C CNN
F 1 "R" V 3450 3000 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0204_L3.6mm_D1.6mm_P7.62mm_Horizontal" V 3380 3000 50  0001 C CNN
F 3 "" H 3450 3000 50  0001 C CNN
	1    3450 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3150 3150 3150 3500
Connection ~ 3150 3500
Wire Wire Line
	3450 3150 3450 3800
Connection ~ 3450 3800
Text HLabel 3150 2300 0    60   Input ~ 0
VD
Text HLabel 3450 2300 0    60   Input ~ 0
VD
Wire Wire Line
	3150 2300 3150 2450
Wire Wire Line
	3450 2300 3450 2450
Wire Wire Line
	3150 2750 3150 2850
Wire Wire Line
	3450 2750 3450 2850
Wire Wire Line
	5200 5700 5900 5700
Connection ~ 5300 5700
Wire Wire Line
	5400 5150 5300 5150
Connection ~ 5300 4850
Wire Wire Line
	5300 3800 5300 3850
Wire Wire Line
	5300 4150 5400 4150
Connection ~ 5300 3800
Wire Wire Line
	4150 2900 4600 2900
Wire Wire Line
	4150 3500 4150 2900
Connection ~ 4250 2900
$EndSCHEMATC
