EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 5
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3100 400  0    118  ~ 0
PUERTOS DE SALIDA Y ENTRADA, EXPANSIONES SERIES SPI, I2C,\nRANURAS DE PUERTOS DE COMUNICACIONES RS232,RS485 Y ETHERNET
Wire Notes Line
	6200 4650 6200 2800
Wire Notes Line
	6200 2800 3450 2800
Wire Notes Line
	3450 2800 3450 4650
Text Notes 4250 3050 0    51   ~ 0
BAHIAS COMPATIBLE\nDIGI
Wire Notes Line
	10450 5500 6600 5500
Wire Notes Line
	6600 5500 6600 2800
Wire Notes Line
	6600 2800 10450 2800
Wire Notes Line
	10450 2800 10450 5500
Text Notes 7700 3150 0    79   ~ 0
BAHIAS PARA  MODULOS\nDE COMUNICACIONES
Wire Notes Line
	3450 4650 6200 4650
$Comp
L Library-cbas:SX1278-LORA U2
U 1 1 611C644B
P 8400 3450
F 0 "U2" H 8475 3715 50  0000 C CNN
F 1 "SX1278-LORA" H 8475 3624 50  0000 C CNN
F 2 "Monitor-RF_MODULE:sx1278" H 8100 3450 50  0001 C CNN
F 3 "/home/cba/kicad/Library-propia/Doc/SX1278ZTR4-lora.pdf" H 8475 3533 50  0000 C CNN
	1    8400 3450
	1    0    0    -1  
$EndComp
Text HLabel 7650 3500 0    60   Input ~ 0
GNDD
Text HLabel 7650 3650 0    60   Input ~ 0
GNDD
Text HLabel 7650 3800 0    60   Input ~ 0
GNDD
Text HLabel 7650 3950 0    60   Input ~ 0
GNDD
Text HLabel 7650 5050 0    60   Input ~ 0
GNDD
Text HLabel 9300 3500 2    60   Input ~ 0
GNDD
Text HLabel 9300 4650 2    60   Input ~ 0
GNDD
Text HLabel 9300 4800 2    60   Input ~ 0
GNDD
Text HLabel 9300 5100 2    60   Input ~ 0
GNDD
Text HLabel 9300 3900 2    60   Input ~ 0
SCK
Text HLabel 9300 4200 2    60   Input ~ 0
SDI
Text HLabel 9300 4050 2    60   Output ~ 0
SDO
Text HLabel 9300 4500 2    60   Input ~ 0
Reset
Text HLabel 9300 4350 2    60   Input ~ 0
CS
Text HLabel 9300 3700 2    60   Input ~ 0
VCCD
Text HLabel 7650 4100 0    60   Output ~ 0
INT
NoConn ~ 7650 4250
NoConn ~ 7650 4400
NoConn ~ 7650 4550
NoConn ~ 7650 4700
NoConn ~ 7650 4850
$Comp
L Device:Antenna AE1
U 1 1 6119FCE2
P 9900 4750
F 0 "AE1" H 9980 4739 50  0000 L CNN
F 1 "Antenna" H 9980 4648 50  0000 L CNN
F 2 "Connector_Coaxial:U.FL_Hirose_U.FL-R-SMT-1_Vertical" H 9900 4750 50  0001 C CNN
F 3 "~" H 9900 4750 50  0001 C CNN
	1    9900 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	9900 4950 9300 4950
$Comp
L Connector:Conn_01x01_Male J3
U 1 1 611AD33E
P 9700 4950
F 0 "J3" H 9808 5131 50  0000 C CNN
F 1 "Conn_01x01_Male" H 9808 5040 50  0000 C CNN
F 2 "Connector_PinHeader_2.00mm:PinHeader_1x01_P2.00mm_Vertical" H 9700 4950 50  0001 C CNN
F 3 "~" H 9700 4950 50  0001 C CNN
	1    9700 4950
	1    0    0    -1  
$EndComp
Connection ~ 9900 4950
$Comp
L Connector:Conn_01x10_Male J1
U 1 1 613DF427
P 3700 3800
F 0 "J1" H 3808 4381 50  0000 C CNN
F 1 "Conn_01x10_Male" H 3808 4290 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x10_P1.27mm_Vertical" H 3700 3800 50  0001 C CNN
F 3 "~" H 3700 3800 50  0001 C CNN
	1    3700 3800
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J2
U 1 1 613DFFCB
P 5050 3500
F 0 "J2" H 5158 4081 50  0000 C CNN
F 1 "Conn_01x04_Male" H 5158 3990 50  0000 C CNN
F 2 "Connector_PinHeader_1.27mm:PinHeader_1x04_P1.27mm_Vertical" H 5050 3500 50  0001 C CNN
F 3 "~" H 5050 3500 50  0001 C CNN
	1    5050 3500
	1    0    0    -1  
$EndComp
Text HLabel 4050 3600 2    63   Input ~ 0
VBAT
Text HLabel 4050 3500 2    63   Input ~ 0
GND
Text HLabel 4050 4000 2    63   Input ~ 0
DIO13-DOUT
Text HLabel 4050 3900 2    63   Input ~ 0
DIO14-DIN
Text HLabel 4050 3800 2    63   Input ~ 0
DIO12-SPI_MISO
Text HLabel 5350 3500 2    63   Input ~ 0
nRESET
Text HLabel 4050 4100 2    63   Input ~ 0
DIO11-I2C-SDA
Text HLabel 5350 3600 2    63   Input ~ 0
DIO4-SPI_MOSI
Text HLabel 5350 3700 2    63   Input ~ 0
DIO2-AD2-SPI_CLK
Text HLabel 4050 3700 2    63   Input ~ 0
DIO1-AD1-I2C_SCL
Text HLabel 5350 3400 2    63   Input ~ 0
DIO0-AD0
Text HLabel 4050 4300 2    63   Input ~ 0
VREF
Wire Wire Line
	4050 3400 3900 3400
Wire Wire Line
	4050 3500 3900 3500
Wire Wire Line
	4050 3600 3900 3600
Wire Wire Line
	4050 3700 3900 3700
Wire Wire Line
	4050 3800 3900 3800
Wire Wire Line
	4050 3900 3900 3900
Wire Wire Line
	4050 4000 3900 4000
Wire Wire Line
	4050 4100 3900 4100
Wire Wire Line
	4050 4300 3900 4300
Wire Wire Line
	5350 3400 5250 3400
Wire Wire Line
	5350 3500 5250 3500
Wire Wire Line
	5350 3600 5250 3600
Wire Wire Line
	5350 3700 5250 3700
Text HLabel 4350 3400 2    60   Input ~ 0
VCCD
$Comp
L Device:D D4
U 1 1 613E5460
P 4200 3400
F 0 "D4" H 4200 3183 50  0000 C CNN
F 1 "D" H 4200 3274 50  0000 C CNN
F 2 "Diode_SMD:D_0402_1005Metric_Castellated" H 4200 3400 50  0001 C CNN
F 3 "~" H 4200 3400 50  0001 C CNN
	1    4200 3400
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 4200 3900 4200
Text HLabel 4050 4200 2    63   Input ~ 0
DIO10-RSSI
$EndSCHEMATC
